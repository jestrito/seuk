<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pageData" required="true"
	type="de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>




<c:forEach items="${pageData.facets}" var="facet">
	<c:set var="skip" value="false" />
	<c:if test="${facet.code eq 'county'}">
		<c:if test="${regionActive eq true }">
			<c:set var="skip" value="true" />
		</c:if>
	</c:if>
	<c:if test="${skip eq false}">
		<c:choose>
			<c:when test="${facet.code eq 'availableInStores'}">
				<nav:facetNavRefinementStoresFacet facetData="${facet}"
					userLocation="${userLocation}" />
			</c:when>
			<c:otherwise>
				<nav:facetNavRefinementFacet facetData="${facet}" />
			</c:otherwise>
		</c:choose>
	</c:if>
</c:forEach>


