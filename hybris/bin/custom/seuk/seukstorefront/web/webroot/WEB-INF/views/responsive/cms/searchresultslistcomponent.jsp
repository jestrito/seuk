<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="title2-wrapper">
	<h3 class="marginb-0" itemprop="headline">
		<spring:theme code="search.page.searchText"
			arguments="${searchPageData.freeTextSearch}" htmlEscape="false" />
	</h3>

	<nav:searchSpellingSuggestion
		spellingSuggestion="${searchPageData.spellingSuggestion}" />
</div>


<nav:pagination top="true" supportShowPaged="${isShowPageAllowed}"
	supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
	searchUrl="${searchPageData.currentQuery.url}"
	numberPagesShown="${numberPagesShown}" />

<div class="remove-ext">
	<div class="row">
		<c:forEach items="${searchPageData.results}" var="product">
			<product:productListerItem product="${product}" />
		</c:forEach>
	</div>
</div>
<!-- <ul class="product__listing product__list"> -->
<%-- 	<c:forEach items="${searchPageData.results}" var="product"> --%>
<%-- 		<product:productListerItem product="${product}" /> --%>
<%-- 	</c:forEach> --%>
<!-- </ul> -->

<!--     <div id="addToCartTitle" class="display-none"> -->
<!--         <div class="add-to-cart-header"> -->
<!--             <div class="headline"> -->
<%--                 <span class="headline-text"><spring:theme code="basket.added.to.basket"/></span> --%>
<!--             </div> -->
<!--         </div> -->
<!--     </div> -->

<nav:pagination top="false" supportShowPaged="${isShowPageAllowed}"
	supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
	searchUrl="${searchPageData.currentQuery.url}"
	numberPagesShown="${numberPagesShown}" />
