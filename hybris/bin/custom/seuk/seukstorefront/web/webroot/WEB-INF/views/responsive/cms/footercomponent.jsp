<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"  %>

<c:url value="https://www.socialenterprise.org.uk/terms-and-conditions/" var="seukTermsLink" />
<c:url value="https://www.socialenterprise.org.uk/privacy-policy/" var="seukPolicyLink" />
<c:url value="https://www.socialenterprise.org.uk/site-map/" var="seukSiteMapLink" />
<c:url value="https://www.socialenterprise.org.uk/contact/" var="seukContactLink" />

<c:url value="https://www.socialenterprise.org.uk/login/" var="seukLoginLink" />

<c:url value="https://www.socialenterprise.org.uk/who-we-are/" var="seukAboutUsLink" />
<c:url value="https://www.socialenterprise.org.uk/sector-jobs-board/" var="seukJoinLink" />
<c:url value="https://www.socialenterprise.org.uk/sector-jobs-board/" var="seukJobsLink" />
<c:url value="https://directdebit.socialenterprise.org.uk/preferences.aspx" var="seukStayInTouchLink" />

<c:url value="https://www.socialenterprise.org.uk/corporate-challenge/" var="seukCorpChallengeLink" />
<c:url value="https://www.socialenterprise.org.uk/social-value/" var="seukSocValLink" />
<c:url value="https://www.socialenterprise.org.uk/social-enterprise-places/" var="seukSocValEntLink" />

<div class="container-fluid">
    <div class="footer__top">
        <div class="row">
            <div class="footer__left col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                	<div class="footer__nav--container col-xs-12 col-sm-3">
						<div class="footer-menu">
	                    	<ul>
								<li>
									<a href="${seukTermsLink}">Terms and Conditions</a>
									<a href="${seukPolicyLink}">SEUK Privacy Policy</a>
									<a href="${seukSiteMapLink}">Sitemap</a>
									<a href="${seukContactLink}">Contact</a>
								</li>
							</ul>
                    	</div>
                    </div>
                    <div class="footer__nav--container col-xs-12 col-sm-3">
                    	<h3>Site info</h3>
                    	<div class="footer-menu">
                    		<ul>
								<li>
									<a href="${seukLoginLink}">Log In</a>
								</li>
							</ul>
                    	</div>
                    </div>
                    <div class="footer__nav--container col-xs-12 col-sm-3">
                        <h3>About</h3>
                        <div class="footer-menu">
                    		<ul>
								<li>
									<a href="${seukAboutUsLink}">About Us</a>
									<a href="${seukLoginLink}">Join SEUK</a>
									<a href="${seukJobsLink}">Jobs</a>
									<a href="${seukStayInTouchLink}">Stay in Touch</a>
								</li>
							</ul>
                    	</div>
                    </div>
                    <div class="footer__nav--container col-xs-12 col-sm-3">
                    	<h3>Our work</h3>
                    	<div class="footer-menu">
                    		<ul>
								<li>
									<a href="${seukCorpChallengeLink}">Corporate Challenge</a>
									<a href="${seukSocValLink}">Social Value</a>
									<a href="${seukSocValEntLink}">Social Enterprise Places</a>
								</li>
							</ul>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub__footer__top">
    	<img src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/02/GetImage-1.png"
    		alt="Social Enterprise UK" title="Social Enterprise UK" class="mediaImage" />
   		<ul class="socialList">
   			<li><a class="twitter" href="https://twitter.com/SocialEnt_UK" target="_blank" style="background-color:#55acee; margin-left:5px;">Twitter</a></li>
   			<li><a class="facebook" href="https://www.facebook.com/Social-Enterprise-UK-136948359726274/" target="_blank" style="background-color:#3b5998; margin-left:5px;">Facebook</a></li>
   			<li><a class="flickr" href="https://www.flickr.com/photos/social_enterprise_coalition/albums" target="_blank" style="background-color:#0063dc; margin-left:5px;">Flickr</a></li>
   			<li><a class="linkedin" href="https://www.linkedin.com/" target="_blank" style="background-color:#0077b5; margin-left:5px;">LinkedIn</a></li>
   			<li><a class="fa-instagram" href="https://www.instagram.com/socialenterpriseuk/" target="_blank" style="background-color:#3f729b; margin-left:5px;">Instagram</a></li>
   		</ul>
   		<p>Social Enterprise UK, The Fire Station, 139 Tooley Street, London, SE1 2HZ Tel: 020 3589 4950</p>
   		<div class="footerLegal">
   			<span style="text-decoration: underline;"></span>
   			Copyright � 2019 | Company Number: 4426564 | All rights reserved.
   		</div>
    </div>
</div>

<div class="row footer__bottom">
    <div class="footer__copyright">
        <h3>Partners</h3>
        <div class="partners">
        	<img class="mediaImage" src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/02/Cabinet-Office.png" title="Cabinet Office" alt="Cabinet Office" width="241" />
        	<img class="mediaImage" src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/06/coop-bw-e1560507745264.png" title="COOP" alt="COOP" width="241" />
        	<img class="mediaImage" src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/02/Department-for-Digital-Culture-Media-and-Sport.png" title="Department for Digital, Culture Media & Sport" alt="Department for Digital, Culture Media & Sport" width="241" />
        	<img class="mediaImage" src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/02/PWC.png" title="PWC" alt="PWC" width="241" />
        	<img class="mediaImage" src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/06/sap-logo-bw-e1560505748222.png" title="SAP" alt="SAP" width="241" />
        </div>
    </div>
</div>
