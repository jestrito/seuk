<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="storepickup"
	tagdir="/WEB-INF/tags/responsive/storepickup"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">
	<div class="container mb-5">
		<div class="row">
			<div class="col-xs-4 pr-0">
				<cms:pageSlot position="ProductLeftRefinements" var="feature"
					element="div" class="search-list-page-left-refinements-slot sticky-top">
					<cms:component component="${feature}" element="div"
						class="search-list-page-left-refinements-component" />
				</cms:pageSlot>
			</div>
			<div class="col-sm-12 col-md-8 pl-0">
				<cms:pageSlot position="SearchResultsListSlot" var="feature"
					element="div" class="search-list-page-right-result-list-slot ml-0">
					<cms:component component="${feature}" element="div"
						class="search-list-page-right-result-list-component" />
				</cms:pageSlot>
			</div>
		</div>
	</div>

	<storepickup:pickupStorePopup />

</template:page>