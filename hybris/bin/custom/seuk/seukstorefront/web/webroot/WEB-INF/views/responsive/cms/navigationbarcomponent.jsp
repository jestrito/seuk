
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set value="${component.styleClass} ${dropDownLayout}"
	var="bannerClasses" />

<%-- <a href="${component.link.url}" title="${component.link.linkName}" --%>
<%-- 	itemprop="url"> <span class="red-clr"><spring:message --%>
<%-- 			code="navbar.searchby" /></span>${component.link.linkName} --%>
<!-- </a> -->

<!-- 					<h1>Test</h1> -->
<%-- ${component} --%>

<c:if test="${not empty component.navigationNode.children}">
	<c:forEach items="${component.navigationNode.children}" var="child1"
		varStatus="loop">
		<li class="menu-item-has-children">
			<a href="#" title="${child1.title}" itemprop="url">
				<span class="red-clr">
					<spring:message code="navbar.searchby" />
				</span>
				${child1.title}
			</a>
			
			<c:if test="${not empty child1.children}">
				<ul class="sub-dropdown">
					<c:forEach items="${child1.children}" var="child2" varStatus="loop">
						<li class="menu-item-has-children">
							<c:url var="child2url" value="#" />
							<c:if test="${not empty child2.links}">
								<c:url var="child2url" value="${child2.links[0].url}" />
							</c:if>
							<a href="${fn:escapeXml(child2url)}" title="${child2.title}" itemprop="url">
								${child2.title} 
<%-- 								<c:if test="${not empty child2.children}"> --%>
<!-- 									<span> -->
<!-- 										<i class="fas fa-caret-right"></i> -->
<!-- 									</span> -->
<%-- 								</c:if> --%>
							</a>
							<c:if test="${not empty child2.children}">
								<ul class="sub-dropdown">
									<c:forEach items="${child2.children}" var="child3" varStatus="loop">
										<c:url var="child3url" value="#" />
										<c:if test="${not empty child3.links}">
											<c:url var="child3url" value="${child3.links[0].url}" />
										</c:if>
										<li class="menu-item-has-children">
											<a href="${fn:escapeXml(child3url)}" title="${child3.title}" itemprop="url">${child3.title}</a>
											<c:if test="${not empty child3.children}">
												<ul class="sub-dropdown">
													<c:forEach items="${child3.children}" var="child4" varStatus="loop">
														<c:url var="child4url" value="#" />
														<c:if test="${not empty child4.links}">
															<c:url var="child4url" value="${child4.links[0].url}" />
														</c:if>
														<li class="menu-item-has-children">
															<a href="${fn:escapeXml(child4url)}" title="${child4.title}" itemprop="url">${child4.title}</a>
														</li>
													</c:forEach>
												</ul>
											</c:if>
										</li>
									</c:forEach>
								</ul>
							</c:if>
							
						</li>
					</c:forEach>
				</ul>
			</c:if>
		</li>
	</c:forEach>
<!-- 	<ul class="sub-dropdown"> -->
<%-- 		<c:forEach items="${component.navigationNode.children}" var="child1" --%>
<%-- 			varStatus="loop"> --%>
<%-- 			<li class="menu-item-has-children"><a href="${child1.url}" --%>
<%-- 				title="${child1.title}" itemprop="url">${child1.title}</a> --%>
<!-- 				<ul class="sub-dropdown"> -->
<!-- 					<li class="menu-item-has-children"><a href="#" -->
<!-- 						title="BLOG LAYOUTS" itemprop="url">BLOG LAYOUTS</a> -->
<!-- 						<ul class="sub-dropdown"> -->
<!-- 							<li><a href="blog-right-sidebar.html" -->
<!-- 								title="BLOG WITH RIGHT SIDEBAR" itemprop="url">BLOG (W.R.S)</a></li> -->
<!-- 							<li><a href="blog-left-sidebar.html" -->
<!-- 								title="BLOG WITH LEFT SIDEBAR" itemprop="url">BLOG (W.L.S)</a></li> -->
<!-- 							<li><a href="blog.html" title="BLOG WITH NO SIDEBAR" -->
<!-- 								itemprop="url">BLOG</a></li> -->
<!-- 						</ul></li> -->
<!-- 					<li class="menu-item-has-children"><a href="#" -->
<!-- 						title="BLOG DETAIL" itemprop="url">BLOG DETAIL</a> -->
<!-- 						<ul class="sub-dropdown"> -->
<!-- 							<li><a href="blog-detail-right-sidebar.html" -->
<!-- 								title="BLOG DETAIL WITH RIGHT SIDEBAR" itemprop="url">BLOG -->
<!-- 									DETAIL (W.R.S)</a></li> -->
<!-- 							<li><a href="blog-detail-left-sidebar.html" -->
<!-- 								title="BLOG DETAIL WITH LEFT SIDEBAR" itemprop="url">BLOG -->
<!-- 									DETAIL (W.L.S)</a></li> -->
<!-- 							<li><a href="blog-detail.html" -->
<!-- 								title="BLOG DETAIL WITH NO SIDEBAR" itemprop="url">BLOG -->
<!-- 									DETAIL</a></li> -->
<!-- 						</ul></li> -->
<!-- 					<li class="menu-item-has-children"><a href="#" -->
<!-- 						title="BLOG FORMATES" itemprop="url">BLOG FORMATES</a> -->
<!-- 						<ul class="sub-dropdown"> -->
<!-- 							<li><a href="blog-detail-video.html" -->
<!-- 								title="BLOG DETAIL WITH VIDEO" itemprop="url">BLOG DETAIL -->
<!-- 									(VIDEO)</a></li> -->
<!-- 							<li><a href="blog-detail-audio.html" -->
<!-- 								title="BLOG DETAIL WITH AUDIO" itemprop="url">BLOG DETAIL -->
<!-- 									(AUDIO)</a></li> -->
<!-- 							<li><a href="blog-detail-carousel.html" -->
<!-- 								title="BLOG DETAIL WITH CAROUSEL" itemprop="url">BLOG DETAIL -->
<!-- 									(CAROUSEL)</a></li> -->
<!-- 						</ul></li> -->
<!-- 				</ul></li> -->
<%-- 			<c:set value="${fn:length(subSection.links)/component.wrapAfter}" --%>
<%-- 				var="subSectionColumns" /> --%>
<%-- 			<c:choose> --%>
<%-- 				<c:when test="${subSectionColumns > 1}"> --%>
<%-- 					<c:set var="totalSubNavigationColumns" --%>
<%-- 						value="${totalSubNavigationColumns + subSectionColumns}" /> --%>
<%-- 				</c:when> --%>

<%-- 				<c:when test="${subSectionColumns < 1}"> --%>
<%-- 					<c:set var="totalSubNavigationColumns" --%>
<%-- 						value="${totalSubNavigationColumns + 1}" /> --%>
<%-- 				</c:when> --%>
<%-- 			</c:choose> --%>

<%-- 			<c:if test="${not empty subSection.title}"> --%>
<%-- 				<c:set var="hasTitleClass" value="has-title" /> --%>
<%-- 			</c:if> --%>
<%-- 		</c:forEach> --%>
<!-- 		<li class="menu-item-has-children"><a href="#" title="BLOG" -->
<!-- 			itemprop="url">BLOG</a> -->
<!-- 			<ul class="sub-dropdown"> -->
<!-- 				<li class="menu-item-has-children"><a href="#" -->
<!-- 					title="BLOG LAYOUTS" itemprop="url">BLOG LAYOUTS</a> -->
<!-- 					<ul class="sub-dropdown"> -->
<!-- 						<li><a href="blog-right-sidebar.html" -->
<!-- 							title="BLOG WITH RIGHT SIDEBAR" itemprop="url">BLOG (W.R.S)</a></li> -->
<!-- 						<li><a href="blog-left-sidebar.html" -->
<!-- 							title="BLOG WITH LEFT SIDEBAR" itemprop="url">BLOG (W.L.S)</a></li> -->
<!-- 						<li><a href="blog.html" title="BLOG WITH NO SIDEBAR" -->
<!-- 							itemprop="url">BLOG</a></li> -->
<!-- 					</ul></li> -->
<!-- 				<li class="menu-item-has-children"><a href="#" -->
<!-- 					title="BLOG DETAIL" itemprop="url">BLOG DETAIL</a> -->
<!-- 					<ul class="sub-dropdown"> -->
<!-- 						<li><a href="blog-detail-right-sidebar.html" -->
<!-- 							title="BLOG DETAIL WITH RIGHT SIDEBAR" itemprop="url">BLOG -->
<!-- 								DETAIL (W.R.S)</a></li> -->
<!-- 						<li><a href="blog-detail-left-sidebar.html" -->
<!-- 							title="BLOG DETAIL WITH LEFT SIDEBAR" itemprop="url">BLOG -->
<!-- 								DETAIL (W.L.S)</a></li> -->
<!-- 						<li><a href="blog-detail.html" -->
<!-- 							title="BLOG DETAIL WITH NO SIDEBAR" itemprop="url">BLOG -->
<!-- 								DETAIL</a></li> -->
<!-- 					</ul></li> -->
<!-- 				<li class="menu-item-has-children"><a href="#" -->
<!-- 					title="BLOG FORMATES" itemprop="url">BLOG FORMATES</a> -->
<!-- 					<ul class="sub-dropdown"> -->
<!-- 						<li><a href="blog-detail-video.html" -->
<!-- 							title="BLOG DETAIL WITH VIDEO" itemprop="url">BLOG DETAIL -->
<!-- 								(VIDEO)</a></li> -->
<!-- 						<li><a href="blog-detail-audio.html" -->
<!-- 							title="BLOG DETAIL WITH AUDIO" itemprop="url">BLOG DETAIL -->
<!-- 								(AUDIO)</a></li> -->
<!-- 						<li><a href="blog-detail-carousel.html" -->
<!-- 							title="BLOG DETAIL WITH CAROUSEL" itemprop="url">BLOG DETAIL -->
<!-- 								(CAROUSEL)</a></li> -->
<!-- 					</ul></li> -->
<!-- 			</ul></li> -->
<!-- 		<li class="menu-item-has-children"><a href="#" -->
<!-- 			title="SPECIAL PAGES" itemprop="url">SPECIAL PAGES</a> -->
<!-- 			<ul class="sub-dropdown"> -->
<!-- 				<li><a href="404.html" title="404 ERROR" itemprop="url">404 -->
<!-- 						ERROR</a></li> -->
<!-- 				<li><a href="search-found.html" title="SEARCH FOUND" -->
<!-- 					itemprop="url">SEARCH FOUND</a></li> -->
<!-- 				<li><a href="search-not-found.html" title="SEARCH NOT FOUND" -->
<!-- 					itemprop="url">SEARCH NOT FOUND</a></li> -->
<!-- 				<li><a href="coming-soon.html" title="COMING SOON" -->
<!-- 					itemprop="url">COMING SOON</a></li> -->
<!-- 				<li><a href="login-register.html" title="LOGIN & REGISTER" -->
<!-- 					itemprop="url">LOGIN & REGISTER</a></li> -->
<!-- 				<li><a href="price-table.html" title="PRICE TABLE" -->
<!-- 					itemprop="url">PRICE TABLE</a></li> -->
<!-- 			</ul></li> -->
<!-- 		<li class="menu-item-has-children"><a href="#" title="GALLERY" -->
<!-- 			itemprop="url">GALLERY</a> -->
<!-- 			<ul class="sub-dropdown"> -->
<!-- 				<li><a href="gallery.html" title="FOOD GALLERY" itemprop="url">FOOD -->
<!-- 						GALLERY</a></li> -->
<!-- 				<li><a href="gallery-detail.html" title="GALLERY DETAIL" -->
<!-- 					itemprop="url">GALLERY DETAIL</a></li> -->
<!-- 			</ul></li> -->
<!-- 		<li><a href="register-reservation.html" -->
<!-- 			title="REGISTER RESERVATION" itemprop="url">REGISTER RESERVATION</a></li> -->
<!-- 		<li><a href="how-it-works.html" title="HOW IT WORKS" -->
<!-- 			itemprop="url">HOW IT WORKS</a></li> -->
<!-- 		<li><a href="dashboard.html" title="USER PROFILE" itemprop="url">USER -->
<!-- 				PROFILE</a></li> -->
<!-- 		<li><a href="about-us.html" title="ABOUT US" itemprop="url">ABOUT -->
<!-- 				US</a></li> -->
<!-- 		<li><a href="food-detail.html" title="FOOD DETAIL" itemprop="url">FOOD -->
<!-- 				DETAIL</a></li> -->
<!-- 	</ul> -->
<%-- </c:if> --%>

<!-- <li -->
<%-- 	class="${fn:escapeXml(bannerClasses)} nav__links--primary <c:if test="${not empty component.navigationNode.children}">nav__links--primary-has__sub js-enquire-has-sub</c:if>"> --%>
<%-- 	<%-- <h1>test - ${component.link.linkName}</h1> --%> 
<%-- 	<cms:component component="${component.link}" evaluateRestriction="true" --%>
<%-- 		element="span" class="nav__link js_nav__link" /> <c:if --%>
<%-- 		test="${not empty component.navigationNode.children}"> --%>

<%-- 		<c:set var="totalSubNavigationColumns" value="${0}" /> --%>

<%-- 		<c:forEach items="${component.navigationNode.children}" --%>
<%-- 			var="subSection" varStatus="loop"> --%>
<%-- 			<c:set value="${fn:length(subSection.links)/component.wrapAfter}" --%>
<%-- 				var="subSectionColumns" /> --%>
<%-- 			<c:choose> --%>
<%-- 				<c:when test="${subSectionColumns > 1}"> --%>
<%-- 					<c:set var="totalSubNavigationColumns" --%>
<%-- 						value="${totalSubNavigationColumns + subSectionColumns}" /> --%>
<%-- 				</c:when> --%>

<%-- 				<c:when test="${subSectionColumns < 1}"> --%>
<%-- 					<c:set var="totalSubNavigationColumns" --%>
<%-- 						value="${totalSubNavigationColumns + 1}" /> --%>
<%-- 				</c:when> --%>
<%-- 			</c:choose> --%>

<%-- 			<c:if test="${not empty subSection.title}"> --%>
<%-- 				<c:set var="hasTitleClass" value="has-title" /> --%>
<%-- 			</c:if> --%>
<%-- 		</c:forEach> --%>

<%-- 		<c:choose> --%>
<%-- 			<c:when --%>
<%-- 				test="${totalSubNavigationColumns > 0 && totalSubNavigationColumns <= 1}"> --%>
<%-- 				<c:set value="col-md-3 col-lg-2" var="subNavigationClass" /> --%>
<%-- 				<c:set value="col-md-12" var="subNavigationItemClass" /> --%>
<%-- 			</c:when> --%>

<%-- 			<c:when test="${totalSubNavigationColumns == 2}"> --%>
<%-- 				<c:set value="col-md-6 col-lg-4" var="subNavigationClass" /> --%>
<%-- 				<c:set value="col-md-6" var="subNavigationItemClass" /> --%>
<%-- 			</c:when> --%>

<%-- 			<c:when test="${totalSubNavigationColumns == 3}"> --%>
<%-- 				<c:set value="col-md-9 col-lg-6" var="subNavigationClass" /> --%>
<%-- 				<c:set value="col-md-4" var="subNavigationItemClass" /> --%>
<%-- 			</c:when> --%>

<%-- 			<c:when test="${totalSubNavigationColumns == 4}"> --%>
<%-- 				<c:set value="col-md-12 col-lg-8" var="subNavigationClass" /> --%>
<%-- 				<c:set value="col-md-3" var="subNavigationItemClass" /> --%>
<%-- 			</c:when> --%>

<%-- 			<c:when test="${totalSubNavigationColumns == 5}"> --%>
<%-- 				<c:set value="col-md-12" var="subNavigationClass" /> --%>
<%-- 				custom grid class required because 1/5th columns aren't supported by bootstrap --%>
<%-- 				<c:set value="column-20-percent" var="subNavigationItemClass" /> --%>
<%-- 			</c:when> --%>

<%-- 			<c:when test="${totalSubNavigationColumns > 5}"> --%>
<%-- 				<c:set value="col-md-12" var="subNavigationClass" /> --%>
<%-- 				<c:set value="col-md-2" var="subNavigationItemClass" /> --%>
<%-- 			</c:when> --%>
<%-- 		</c:choose> --%>
<%-- 		<c:if test="${not empty component.navigationNode.children}"> --%>
<!-- 			<span -->
<!-- 				class="glyphicon  glyphicon-chevron-right hidden-md hidden-lg nav__link--drill__down js_nav__link--drill__down"></span> -->
<%-- 		</c:if> --%>
<%-- 		<div class="sub__navigation js_sub__navigation ${subNavigationClass}"> --%>
<!-- 			<a class="sm-back js-enquire-sub-close hidden-md hidden-lg" href="#">Back</a> -->
<!-- 			<div class="row"> -->
<%-- 				<c:forEach items="${component.navigationNode.children}" var="child"> --%>
<%-- 					<c:if test="${child.visible}"> --%>
<%-- 						<c:forEach items="${child.links}" step="${component.wrapAfter}" --%>
<%-- 							var="childlink" varStatus="i"> --%>
<%-- 							for a large amount of links (depending on what wrapAfter is set to) that would exceed 6 columns, insert a clearfix div to have the next row properly aligned --%>
<%-- 							<c:if --%>
<%-- 								test="${i.index != 0 && i.index % (6*component.wrapAfter) == 0}"> --%>
<!-- 								<div class="clearfix hidden-sm-down"></div> -->
<%-- 							</c:if> --%>

<%-- 							<div class="sub-navigation-section ${subNavigationItemClass}"> --%>
<%-- 								only add title on first loop for each sub-section --%>
<%-- 								<c:if test="${i.index == 0 && not empty child.title}"> --%>
<%-- 									<div class="title">${fn:escapeXml(child.title)}</div> --%>
<%-- 								</c:if> --%>

<%-- 								<ul class="sub-navigation-list ${hasTitleClass}"> --%>
<%-- 									<c:forEach items="${child.links}" var="childlink" --%>
<%-- 										begin="${i.index}" end="${i.index + component.wrapAfter - 1}"> --%>
<%-- 										<c:if test="${fn:contains(childlink.uid, 'BrowseAll')}"> --%>
<!-- 											<span class="text-uppercase"> -->
<%-- 										</c:if> --%>
<%-- 										<cms:component component="${childlink}" --%>
<%-- 											evaluateRestriction="true" element="li" --%>
<%-- 											class="nav__link--secondary" /> --%>
<%-- 										<c:if test="${fn:contains(childlink.uid, 'BrowseAll')}"> --%>
<!-- 											</span> -->
<%-- 										</c:if> --%>
<%-- 									</c:forEach> --%>
<!-- 								</ul> -->

<!-- 							</div> -->
<%-- 						</c:forEach> --%>
<%-- 					</c:if> --%>
<%-- 				</c:forEach> --%>
<!-- 			</div> -->
<!-- 		</div> -->
</c:if>
