<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="facetData" required="true"
	type="de.hybris.platform.commerceservices.search.facetdata.FacetData"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty facetData.values}">
	<ycommerce:testId code="facetNav_title_${facetData.name}">
		<div class="widget style2 Search_filters wow fadeIn"
			data-wow-delay="0.2s">
			<h4 class="widget-title2 sudo-bg-red" itemprop="headline">
				<spring:theme code="search.nav.facetTitle"
					arguments="${facetData.name}" />
			</h4>
			<div class="widget-data">
				<ul>
					<c:forEach items="${facetData.values}" var="facetValue">
						<c:set var="parent" value="false" />
						<c:forEach var="item" items="${firstLevel}">
							<c:if test="${item eq fn:escapeXml(facetValue.code)}">
								<c:set var="parent" value="true" />
							</c:if>
						</c:forEach>
						<c:if test="${facetData.code eq 'category'}">
							<c:if test="${parent}">
								<li><c:if test="${not facetData.multiSelect}">
										<c:url value="${facetValue.query.url}"
											var="facetValueQueryUrl" />
										<a class="text-pelorous"
											title="${fn:escapeXml(facetValue.name)}" itemprop="url"
											href="${fn:escapeXml(facetValueQueryUrl)}">
												
												${fn:escapeXml(facetValue.name)}
												<span class="float-right"> <ycommerce:testId code="facetNav_count">${facetValue.count}</ycommerce:testId>
												</span>

										</a>
									</c:if></li>
							</c:if>
						</c:if>
						<c:if test="${facetData.code ne 'category'}">
							<li><c:if test="${not facetData.multiSelect}">
									<c:url value="${facetValue.query.url}" var="facetValueQueryUrl" />
									<a class="text-pelorous"
										href="${fn:escapeXml(facetValueQueryUrl)}">
											${fn:escapeXml(facetValue.name)}
												<span class="float-right"> <ycommerce:testId code="facetNav_count">${facetValue.count}</ycommerce:testId>
											</span>
									</a>
								</c:if></li>
						</c:if>
					</c:forEach>
				</ul>
			</div>
		</div>

		<!-- 		<article class="card-group-item"> -->
		<!-- 			<header class="card-header"> -->
		<!-- 				<a class="" aria-expanded="true" href="#" data-toggle="collapse" -->
		<!-- 					data-target="#${facetData.code}"> <i -->
		<!-- 					class="icon-action fa fa-chevron-down"></i> -->
		<!-- 					<h6 class="title text-blulagoon"> -->
		<%-- 						<spring:theme code="search.nav.facetTitle" --%>
		<%-- 							arguments="${facetData.name}" /> --%>
		<!-- 					</h6> -->
		<!-- 				</a> -->
		<!-- 			</header> -->
		<!-- 			<div style="" class="filter-content collapse show" -->
		<%-- 				id="${facetData.code}"> --%>
		<!-- 				<div class="card-body"> -->
		<%-- 					<c:if test="${not empty facetData.topValues}"> --%>
		<!-- 						<ul -->
		<!-- 							class="list-unstyled list-lg js-facet-list js-facet-top-values text-pelorous"> -->
		<%-- 							<c:forEach items="${facetData.topValues}" var="facetValue"> --%>
		<%-- 								<li><c:if test="${facetData.multiSelect}"> --%>
		<!-- 										<form action="#" method="get"> -->
		<!-- 											facetValue.query.query.value and searchPageData.freeTextSearch are html output encoded in the backend -->
		<!-- 											<input type="hidden" name="q" -->
		<%-- 												value="${facetValue.query.query.value}" /> <input --%>
		<!-- 												type="hidden" name="text" -->
		<%-- 												value="${searchPageData.freeTextSearch}" /> <label> --%>
		<!-- 												<input class="facet__list__checkbox" type="checkbox" -->
		<%-- 												${facetValue.selected ? 'checked="checked"' : ''} --%>
		<!-- 												class="facet-checkbox" /> <span class="facet__list__label"> -->
		<!-- 													<span class="facet__list__mark"></span> <span -->
		<!-- 													class="facet__list__text"> -->
		<%-- 														${fn:escapeXml(facetValue.name)} <ycommerce:testId --%>
		<%-- 															code="facetNav_count"> --%>
		<%-- 															<span class="facet__value__count"><spring:theme --%>
		<%-- 																	code="search.nav.facetValueCount" --%>
		<%-- 																	arguments="${facetValue.count}" /></span> --%>
		<%-- 														</ycommerce:testId> --%>
		<!-- 												</span> -->
		<!-- 											</span> -->
		<!-- 											</label> -->
		<!-- 										</form> -->
		<%-- 									</c:if> <c:if test="${not facetData.multiSelect}"> --%>
		<%-- 										<c:url value="${facetValue.query.url}" --%>
		<%-- 											var="facetValueQueryUrl" /> --%>
		<!-- 										<span class="facet__text"> searchPageData.freeTextSearch is html output encoded in the backend -->
		<!-- 											<a -->
		<%-- 											href="${fn:escapeXml(facetValueQueryUrl)}&amp;text=${searchPageData.freeTextSearch}">${fn:escapeXml(facetValue.name)}</a>&nbsp; --%>
		<%-- 											<ycommerce:testId code="facetNav_count"> --%>
		<%-- 												<span class="facet__value__count"><spring:theme --%>
		<%-- 														code="search.nav.facetValueCount" --%>
		<%-- 														arguments="${facetValue.count}" /></span> --%>
		<%-- 											</ycommerce:testId> --%>
		<!-- 										</span> -->
		<%-- 									</c:if></li> --%>
		<%-- 							</c:forEach> --%>
		<!-- 						</ul> -->
		<%-- 					</c:if> --%>
		<!-- 					<ul -->
		<%-- 						class="list-unstyled list-lg js-facet-list <c:if test="${not empty facetData.topValues}">facet__list--hidden js-facet-list-hidden</c:if> text-pelorous"> --%>
		<%-- 						<c:if test="${facetData.code eq 'category'}"> --%>
		<%-- 							<c:forEach items="${firstLevel}" var="level"> --%>
		<%-- 															<h6>${level}</h6> --%>
		<%-- 							</c:forEach> --%>
		<%-- 						</c:if> --%>
		<%-- 						<c:forEach items="${facetData.values}" var="facetValue"> --%>
		<%-- 							<c:set var="parent" value="false" /> --%>
		<%-- 							<c:forEach var="item" items="${firstLevel}"> --%>
		<%-- 								<c:if test="${item eq fn:escapeXml(facetValue.code)}"> --%>
		<%-- 									<c:set var="parent" value="true" /> --%>
		<%-- 								</c:if> --%>
		<%-- 							</c:forEach> --%>
		<%-- 							<c:if test="${facetData.code eq 'category'}"> --%>
		<%-- 																	<h6>Parent = ${parent}, code = ${fn:escapeXml(facetValue.code)}</h6> --%>
		<%-- 								<c:if test="${parent}"> --%>
		<%-- 									<li><c:if test="${facetData.multiSelect}"> --%>
		<%-- 											<ycommerce:testId code="facetNav_selectForm"> --%>
		<!-- 												<form action="#" method="get"> -->
		<!-- 													facetValue.query.query.value and searchPageData.freeTextSearch are html output encoded in the backend -->
		<!-- 													<input type="hidden" name="q" -->
		<%-- 														value="${facetValue.query.query.value}" /> <input --%>
		<!-- 														type="hidden" name="text" -->
		<%-- 														value="${searchPageData.freeTextSearch}" /> <label> --%>
		<!-- 														<input type="checkbox" -->
		<%-- 														${facetValue.selected ? 'checked="checked"' : ''} --%>
		<!-- 														class="facet__list__checkbox js-facet-checkbox sr-only" /> -->
		<!-- 														<span class="facet__list__label"> <span -->
		<!-- 															class="facet__list__mark"></span> <span -->
		<!-- 															class="facet__list__text"> -->
		<%-- 																${fn:escapeXml(facetValue.name)}<span --%>
		<%-- 																class="float-right badge badge-light round"> <ycommerce:testId --%>
		<%-- 																		code="facetNav_count">${facetValue.count}" /> --%>
		<%-- 															</ycommerce:testId></span> --%>
		<!-- 														</span> -->
		<!-- 													</span> -->
		<!-- 													</label> -->
		<!-- 												</form> -->
		<%-- 											</ycommerce:testId> --%>
		<%-- 										</c:if> <c:if test="${not facetData.multiSelect}"> --%>
		<%-- 											<c:url value="${facetValue.query.url}" --%>
		<%-- 												var="facetValueQueryUrl" /> --%>
		<!-- 											<span class="facet__text"> <a class="text-pelorous" -->
		<%-- 												href="${fn:escapeXml(facetValueQueryUrl)}">${fn:escapeXml(facetValue.name)} --%>
		<!-- 													<span -->
		<%-- 													class="float-right badge badge-light round text-prussianblue"><ycommerce:testId --%>
		<%-- 															code="facetNav_count">${facetValue.count} --%>
		<%-- 														</ycommerce:testId> </span> --%>
		<!-- 											</a> -->
		<!-- 											</span> -->

		<%-- 										</c:if></li> --%>
		<%-- 								</c:if> --%>
		<%-- 							</c:if> --%>
		<%-- 							<c:if test="${facetData.code ne 'category'}"> --%>
		<%-- 								<li><c:if test="${facetData.multiSelect}"> --%>
		<%-- 										<ycommerce:testId code="facetNav_selectForm"> --%>
		<!-- 											<form action="#" method="get"> -->
		<!-- 												facetValue.query.query.value and searchPageData.freeTextSearch are html output encoded in the backend -->
		<!-- 												<input type="hidden" name="q" -->
		<%-- 													value="${facetValue.query.query.value}" /> <input --%>
		<!-- 													type="hidden" name="text" -->
		<%-- 													value="${searchPageData.freeTextSearch}" /> <label> --%>
		<!-- 													<input type="checkbox" -->
		<%-- 													${facetValue.selected ? 'checked="checked"' : ''} --%>
		<!-- 													class="facet__list__checkbox js-facet-checkbox sr-only" /> -->
		<!-- 													<span class="facet__list__label"> <span -->
		<!-- 														class="facet__list__mark"></span> <span -->
		<!-- 														class="facet__list__text"> -->
		<%-- 															${fn:escapeXml(facetValue.name)}<span --%>
		<%-- 															class="float-right badge badge-light round text-prussianblue"><ycommerce:testId --%>
		<%-- 																	code="facetNav_count">${facetValue.count} --%>
		<%-- 														</ycommerce:testId> </span> --%>
		<!-- 													</span> -->
		<!-- 												</span> -->
		<!-- 												</label> -->
		<!-- 											</form> -->
		<%-- 										</ycommerce:testId> --%>
		<%-- 									</c:if> <c:if test="${not facetData.multiSelect}"> --%>
		<%-- 										<c:url value="${facetValue.query.url}" --%>
		<%-- 											var="facetValueQueryUrl" /> --%>
		<!-- 										<span class="facet__text"> <a class="text-pelorous" -->
		<%-- 											href="${fn:escapeXml(facetValueQueryUrl)}">${fn:escapeXml(facetValue.name)} --%>
		<!-- 												<span -->
		<%-- 												class="float-right badge badge-light round text-prussianblue"><ycommerce:testId --%>
		<%-- 														code="facetNav_count">${facetValue.count} --%>
		<%-- 														</ycommerce:testId> </span> --%>
		<!-- 										</a> -->
		<!-- 										</span> -->
		<%-- 									</c:if></li> --%>
		<%-- 							</c:if> --%>
		<%-- 						</c:forEach> --%>
		<!-- 					</ul> -->

		<%-- 					<c:if test="${not empty facetData.topValues}"> --%>
		<!-- 						<span class="facet__values__more js-more-facet-values"> <a -->
		<%-- 							href="#" class="js-more-facet-values-link"><spring:theme --%>
		<%-- 									code="search.nav.facetShowMore_${facetData.code}" /></a> --%>
		<!-- 						</span> -->
		<!-- 						<span class="facet__values__less js-less-facet-values"> <a -->
		<%-- 							href="#" class="js-less-facet-values-link"><spring:theme --%>
		<%-- 									code="search.nav.facetShowLess_${facetData.code}" /></a> --%>
		<!-- 						</span> -->
		<%-- 					</c:if> --%>
		<!-- 				</div> -->
		<!-- 				card-body.// -->
		<!-- 			</div> -->
		<!-- 			<!-- collapse .// -->
		<!-- 		</article> -->
		<!-- card-group-item.// -->
	</ycommerce:testId>
</c:if>
