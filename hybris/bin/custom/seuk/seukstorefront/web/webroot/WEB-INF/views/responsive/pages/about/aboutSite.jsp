<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="about" tagdir="/WEB-INF/tags/responsive/about"%>

<c:url value="/j_spring_security_check" var="loginActionUrl" />

<div class="login-section">
	<about:about actionNameKey="login.login" />
</div>