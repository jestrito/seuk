<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:url value="/test" var="test"/>

<template:page pageTitle="${pageTitle}">
   <%-- <cms:pageSlot position="Section2" var="feature" element="div" class="horizontal-center row no-margin" >
        <cms:component component="${feature}" element="div" class="no-space yComponentWrapper"/>
   </cms:pageSlot> --%>
   
   <div class="banner-img-parallax"></div>
   
   <!-- <div class="row" style="postion: relative">
     	<div class="banner-image">
			<img src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/08/Website-header-image.png" />
     	</div>
    </div> -->
    
    <div class="row" style="width: 100%; margin: 0 auto; position: absolute; top: 350px;">
		<div class="col-sm-12">
			<div class="site-search">
				<cms:pageSlot position="SearchBox" var="component">
					<cms:component component="${component}" element="div"/>
				</cms:pageSlot>
			</div>
		</div>
	</div>
   	
   	<section style="background: #fff;">
   		<div class="block remove-bottom">
   			<div class="container">
   				<div class="row">
   					<div class="welcome-sec">
	   					<div class="row">
	   						<div class="col-md-6 col-sm-6 col-lg-6">
	   							<div class="welcome-secinfo">
	   								<cms:pageSlot position="Section1" var="feature">
								      <cms:component component="${feature}" />
								  	</cms:pageSlot>
	   							</div>
	   						</div>
	   						<div class="col-md-6 col-sm-6 col-lg-6">
	   							<cms:pageSlot position="SBSection3" var="feature">
							      <cms:component component="${feature}" />
							  	</cms:pageSlot>
	   						</div>
	   					</div>
   					</div>
   				</div>
   			</div>
   		</div>
   	</section>
   	
   	<section class="homeFeaturesWrapper">
		<div class="homeFeatures">
			<div class="homeboxeswrap">
				<div class="homeBox-title">
					<h2>MORE ABOUT US</h2>
				</div>
				<section class="homeFeature homeBox1">
					<a class="" title="Read: Induction video" href="https://www.socialenterprise.org.uk/induction-video/">
						<img src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/08/Chris-and-Michael.png" alt="Induction video" class="banner homeBoxImage"/>
					</a>
					<div class="homeFeatureDetailsWrapper">
						<h4><a href="https://www.socialenterprise.org.uk/induction-video/">Induction video</a></h4>
						<p>
							We have been working with SEUK members and our Patrons, Michael Sheen and Chris Addison, to put together an induction film with supporting materials to help employees of social enterprises understand the impact of the sector and why working for a social enterprise is different, and special.
						</p>
						<p></p>
					</div>
				</section>
				<section class="homeFeature homeBox2" onclick="window.location = 'https://www.socialenterprise.org.uk/seuk-members/'">
					<a class="" title="Read: SEUK Members" href="https://www.socialenterprise.org.uk/seuk-members/">
						<img src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/02/SEUK-Members.png" alt="SEUK Members" class="banner homeBoxImage"/>
					</a>
					<div class="homeFeatureDetailsWrapper">
						<h4><a href="https://www.socialenterprise.org.uk/induction-video/">SEUK Members</a></h4>
						<p>
							We have members all over the UK as well as in various parts of the world. You can view all our members on a map as well as searching for particular organisations and locations.
						</p>
						<p></p>
					</div>
				</section>
				<section class="homeFeature homeBox3" onclick="window.location = 'https://www.socialenterprise.org.uk/policy-and-research-reports/front-and-centre-putting-social-value-at-the-heart-of-inclusive-growth/'">
					<a class="" title="Read: Front and Centre - Putting Social Value at the Heart of Inclusive Growth" href="https://www.socialenterprise.org.uk/policy-and-research-reports/front-and-centre-putting-social-value-at-the-heart-of-inclusive-growth/">
						<img src="https://www.socialenterprise.org.uk/wp-content/uploads/2019/05/front-and-centre.png" alt="Front and Centre - Putting Social Value at the Heart of Inclusive Growth" class="banner homeBoxImage"/>
					</a>
					<div class="homeFeatureDetailsWrapper">
						<h4><a href="https://www.socialenterprise.org.uk/induction-video/">Front and Centre - Putting Social Value at the Heart of Inclusive Growth</a></h4>
						<p>
							Our latest research report calls for a rethink on how public bodies use social value, urging it to be placed front and centre of how local and central government works.
						</p>
						<p></p>
					</div>
				</section>
			</div>
		</div>
	</section>
	    
    <div class="row no-margin">
        <div class="col-xs-12 col-md-6 no-space">
            <cms:pageSlot position="Section2A" var="feature" element="div" class="row no-margin">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-6 no-space yComponentWrapper"/>
            </cms:pageSlot>
        </div>
        <div class="col-xs-12 col-md-6 no-space">
            <cms:pageSlot position="Section2B" var="feature" element="div" class="row no-margin">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-6 no-space yComponentWrapper"/>
            </cms:pageSlot>
        </div>
        <div class="col-xs-12">
            <cms:pageSlot position="Section2C" var="feature" element="div" class="landingLayout2PageSection2C">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
        </div>
    </div>
	
    <cms:pageSlot position="Section3" var="feature" element="div" class="row no-margin" >
        <cms:component component="${feature}" element="div" class="no-space yComponentWrapper"/>
    </cms:pageSlot>

    <cms:pageSlot position="Section4" var="feature" element="div" class="row no-margin">
        <cms:component component="${feature}" element="div" class="col-xs-6 col-md-3 no-space yComponentWrapper"/>
    </cms:pageSlot>

    <cms:pageSlot position="Section5" var="feature" element="div">
        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
    </cms:pageSlot>

</template:page>
