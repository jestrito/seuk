<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="product-details page-title row vertical-center">
	<ycommerce:testId
		code="productDetails_productNamePrice_label_${product.code}">
		<div class="col-sm-2">
			<product:productPrimaryImage product="${product}" format="thumbnail" />
		</div>
		<div class="col-sm-10">
			<h1 class="">${fn:escapeXml(product.name)}</h1>
		</div>
		<%-- 						<div class="name">${fn:escapeXml(product.name)} --%>
		<!--span class="sku">ID</span>
			<span class="code">${fn:escapeXml(product.code)}</span-->
		<!-- 		</div> -->
	</ycommerce:testId>
	<%-- 	<product:productReviewSummary product="${product}" showLinks="true"/> --%>
</div>
<div class="row bg-light profile rounded p-5">
	<div class="col-sm-12 mx-3">
		<h4>
			<strong>Profile:</strong>
		</h4>
		<div class="product-details">
			<div class="description">
				${ycommerce:sanitizeHTML(product.description)}</div>
		</div>
	</div>
	<div class="col-sm-12 mx-3">

		<div class="row mt-3">
			<div class="col-md-6">
				<div class="col-sm-12">
					<h4>
						<strong>Main Customer:</strong>
					</h4>
					<div class="product-details">
						<div class="description">
							${ycommerce:sanitizeHTML(product.market)}</div>
					</div>
				</div>
				<div class="col-sm-12 mt-3">
					<h4>
						<strong>Trading Activities:</strong>
					</h4>
					<div class="product-details">
						<div class="description">
							${ycommerce:sanitizeHTML(product.tradingActivity)}</div>
					</div>
				</div>
				<div class="col-sm-12 mt-3">
					<h4>
						<strong>Social and Environmental Mission:</strong>
					</h4>
					<div class="product-details">
						<div class="description">
							${ycommerce:sanitizeHTML(product.mission)}</div>
					</div>
				</div>


			</div>
			<div class="col-md-6">
				<div class="col-sm-12">
					<h4>
						<strong>Address:</strong>
					</h4>
					<div class="product-details">
						<div class="description">
							${ycommerce:sanitizeHTML(product.line1)},
							${ycommerce:sanitizeHTML(product.line2)}<br>
							${ycommerce:sanitizeHTML(product.city)}<br>
							${ycommerce:sanitizeHTML(product.county)}<br>
							${ycommerce:sanitizeHTML(product.postCode)}<br>
							${ycommerce:sanitizeHTML(product.region)}
						</div>
					</div>
				</div>
				<div class="col-sm-12 mt-3">
					<h4>
						<strong> <a
							class="btn red-bg brd-rd4 rounded-pill text-white"
							data-toggle="collapse" href="#contactUs" role="button"
							aria-expanded="false" aria-controls="contactUs">Contact Us:</a>
						</strong>
					</h4>
					<div class="product-details collapse" data-parent="#contactUs" id="contactUs">
						<div class="description" >
							Website: <a
								href="https://${ycommerce:sanitizeHTML(product.website)}">${ycommerce:sanitizeHTML(product.website)}</a><br>
							Telephone: ${ycommerce:sanitizeHTML(product.telephone)}<br>
							Email: ${ycommerce:sanitizeHTML(product.email)}<br> Contact:
							${ycommerce:sanitizeHTML(product.contact)}<br>
							<h3>
								<a href="https://${ycommerce:sanitizeHTML(product.facebook)}"><i
									class="fab fa-facebook-square"></i></a> <a
									href="https://${ycommerce:sanitizeHTML(product.twitter)}"><i
									class="fab fa-twitter-square"></i></a>
							</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 	<div class="col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 col-lg-4"> -->
	<%-- 		<%-- 		<product:productImagePanel galleryImages="${galleryImages}" /> --%>
	<!-- 	</div> -->
	<!-- 	<div class="clearfix hidden-sm hidden-md hidden-lg"></div> -->
	<!-- 	<div class="col-sm-6 col-lg-8 "> -->
	<!-- 		<div class="product-main-info"> -->
	<!-- 			<div class="row"> -->
	<!-- 				<div class="col-12"> -->
	<!-- 					<br> <br> -->
	<!-- 					<h4> -->
	<!-- 						<strong>Address:</strong> -->
	<!-- 					</h4> -->
	<!-- 					<div class="product-details"> -->
	<%-- 												<product:productPromotionSection product="${product}"/> --%>
	<%-- 												<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}"> --%>
	<%-- 													<product:productPricePanel product="${product}" /> --%>
	<%-- 												</ycommerce:testId> --%>
	<!-- 						<div class="description"> -->
	<%-- 							${ycommerce:sanitizeHTML(product.street)}<br> --%>
	<%-- 							${ycommerce:sanitizeHTML(product.city)}<br> --%>
	<%-- 							${ycommerce:sanitizeHTML(product.county)}<br> --%>
	<%-- 							${ycommerce:sanitizeHTML(product.postCode)}<br> --%>
	<%-- 							${ycommerce:sanitizeHTML(product.region)}<br> <br> --%>
	<%-- 							Website: <a href="${ycommerce:sanitizeHTML(product.website)}">${ycommerce:sanitizeHTML(product.website)}</a> --%>
	<!-- 						</div> -->
	<!-- 					</div> -->

	<!-- 					<br> -->
	<!-- 					<h4> -->
	<!-- 						<strong>Trading Activities:</strong> -->
	<!-- 					</h4> -->
	<!-- 					<div class="product-details"> -->
	<%-- 												<product:productPromotionSection product="${product}"/> --%>
	<%-- 												<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}"> --%>
	<%-- 													<product:productPricePanel product="${product}" /> --%>
	<%-- 												</ycommerce:testId> --%>
	<!-- 						<div class="description"> -->
	<%-- 							${ycommerce:sanitizeHTML(product.tradingActivity)}<br> --%>

	<!-- 						</div> -->
	<!-- 					</div> -->

	<!-- 					<br> -->
	<!-- 					<h4> -->
	<!-- 						<strong>Social and Environmental Mission:</strong> -->
	<!-- 					</h4> -->
	<!-- 					<div class="product-details"> -->
	<%-- 												<product:productPromotionSection product="${product}"/> --%>
	<%-- 												<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}"> --%>
	<%-- 													<product:productPricePanel product="${product}" /> --%>
	<%-- 												</ycommerce:testId> --%>
	<!-- 						<div class="description"> -->
	<%-- 							${ycommerce:sanitizeHTML(product.mission)}<br> --%>
	<!-- 						</div> -->
	<!-- 					</div> -->

	<!-- 					<br> -->
	<!-- 					<h4> -->
	<!-- 						<strong>Main Customer:</strong> -->
	<!-- 					</h4> -->
	<!-- 					<div class="product-details"> -->
	<%-- 												<product:productPromotionSection product="${product}"/> --%>
	<%-- 												<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}"> --%>
	<%-- 													<product:productPricePanel product="${product}" /> --%>
	<%-- 												</ycommerce:testId> --%>
	<!-- 						<div class="description"> -->
	<%-- 							${ycommerce:sanitizeHTML(product.market)}<br> --%>
	<!-- 						</div> -->
	<!-- 					</div> -->

	<!-- 					<br> -->
	<!-- 					<h4> -->
	<!-- 						<strong>Profile:</strong> -->
	<!-- 					</h4> -->
	<!-- 					<div class="product-details"> -->
	<%-- 												<product:productPromotionSection product="${product}"/> --%>
	<%-- 												<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}"> --%>
	<%-- 													<product:productPricePanel product="${product}" /> --%>
	<%-- 												</ycommerce:testId> --%>
	<!-- 						<div class="description"> -->
	<%-- 							${ycommerce:sanitizeHTML(product.description)}<br> --%>
	<!-- 						</div> -->
	<!-- 					</div> -->
	<!-- 				</div> -->

	<!-- 				div class="col-sm-12 col-md-9 col-lg-6">
<!-- 					<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select"> -->
	<!-- 						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/> -->
	<!-- 					</cms:pageSlot> -->
	<!-- 					<cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select"> -->
	<!-- 						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/> -->
	<!-- 					</cms:pageSlot> -->
	<!-- 				</div-->
	<!-- 			</div> -->
	<!-- 		</div> -->

	<!-- 	</div> -->
</div>