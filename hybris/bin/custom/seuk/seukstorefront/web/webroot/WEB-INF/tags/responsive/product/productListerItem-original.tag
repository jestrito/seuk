<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<spring:theme code="text.addToCart" var="addToCartText" />
<c:url value="${product.url}" var="productUrl" />

<c:set value="${not empty product.potentialPromotions}"
	var="hasPromotion" />

<c:set value="product__list--item" var="productTagClasses" />
<c:forEach var="tag" items="${product.tags}">
	<c:set value="${productTagClasses} tag-${tag}" var="productTagClasses" />
</c:forEach>

<li class="${fn:escapeXml(productTagClasses)}"><ycommerce:testId
		code="test_searchPage_wholeProduct">
		<div class="row">
			<div class="col-2 list-wrapper text-center">
				<a class="product__list--thumb mr-0" href="${fn:escapeXml(productUrl)}"
					title="${fn:escapeXml(product.name)}"> <product:productPrimaryImage
						product="${product}" format="thumbnail" />
				</a>
			</div>
			<div class="col-10">
				<ycommerce:testId code="searchPage_productName_link_${product.code}">
				<h5>${ycommerce:sanitizeHTML(product.name)}</h5>
					<a class="product__list--name ml-1 stretched-link"
						href="${fn:escapeXml(productUrl)}"></a>
					<%-- 			<br> <strong>${ycommerce:sanitizeHTML(product.headOffice)}</strong> --%>
				</ycommerce:testId>

				<div class="product__list--price-panel">
					<c:if test="${not empty product.potentialPromotions}">
						<div class="product__listing--promo">
							<c:forEach items="${product.potentialPromotions}" var="promotion">
						${ycommerce:sanitizeHTML(promotion.description)}
					</c:forEach>
						</div>
					</c:if>

					<ycommerce:testId code="searchPage_price_label_${product.code}">
						<div class="product__listing--price">
							<product:productListerItemPrice product="${product}" />
						</div>
					</ycommerce:testId>
				</div>
				<c:if test="${not empty product.headOffice}">
					<div class="product__listing--description ml-2">
						<strong>${ycommerce:sanitizeHTML(product.headOffice)}</strong>
					</div>
				</c:if>

				<c:if test="${not empty product.mission}">
					<br>
					<div class="product__listing--description ml-2">Mission:
						${ycommerce:sanitizeHTML(product.mission)}</div>
				</c:if>

				<c:if test="${not empty product.tradingActivity}">
					<div class="product__listing--description ml-2">Trading
						Activity: <span class="badge badge-pill badge-light text-atlantis">${ycommerce:sanitizeHTML(product.tradingActivity)}</span></div>
				</c:if>


				<c:set var="product" value="${product}" scope="request" />
				<c:set var="addToCartText" value="${addToCartText}" scope="request" />
				<c:set var="addToCartUrl" value="${addToCartUrl}" scope="request" />
				<div class="addtocart">
					<div id="actions-container-for-${fn:escapeXml(component.uid)}"
						class="row">
						<%-- 				<action:actions element="div" parentComponent="${component}"  /> --%>
					</div>
				</div>

			</div>
		</div>


	</ycommerce:testId></li>







