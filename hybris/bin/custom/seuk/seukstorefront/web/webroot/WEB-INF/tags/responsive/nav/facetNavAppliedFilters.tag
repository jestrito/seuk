<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pageData" required="true"
	type="de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty pageData.breadcrumbs}">
	<div class="widget style2 Search_filters wow fadeIn"
		data-wow-delay="0.2s">
		<h4 class="widget-title2 sudo-bg-red" itemprop="headline">
			<spring:theme code="search.nav.applied.facets" />
		</h4>
		<div class="widget-data">
			<ul>
				<c:forEach items="${pageData.breadcrumbs}" var="breadcrumb">
					<li>
						<c:url value="${breadcrumb.removeQuery.url}" var="removeQueryUrl" /> 
						<a href="${fn:escapeXml(removeQueryUrl)}" itemprop="url">
							<strong>${fn:escapeXml(breadcrumb.facetValueName)}</strong>
								<span class="float-right">
									<i class="fas fa-times-circle"></i>
								</span>
						</a>
					</li>
				</c:forEach>
			</ul>
		</div>
	</div>


<!-- 	<article class="card-group-item"> -->
<!-- 		<header class="card-header"> -->
<!-- 			<a class="" aria-expanded="true" href="#" data-toggle="collapse" -->
<!-- 				data-target="#applied"> <i -->
<!-- 				class="icon-action fa fa-chevron-down"></i> -->
<!-- 				<h6 class="title text-blulagoon"> -->
<%-- 					<spring:theme code="search.nav.applied.facets" /> --%>
<!-- 				</h6> -->
<!-- 			</a> -->
<!-- 		</header> -->
<!-- 		<div style="" class="filter-content collapse show" id="applied"> -->
<!-- 			<div class="card-body"> -->
<!-- 				<ul class="list-unstyled list-lg text-ruby"> -->
<%-- 					<c:forEach items="${pageData.breadcrumbs}" var="breadcrumb"> --%>
<%-- 						<li><c:url value="${breadcrumb.removeQuery.url}" --%>
<%-- 								var="removeQueryUrl" /> <strong>${fn:escapeXml(breadcrumb.facetValueName)}</strong>&nbsp;<a --%>
<%-- 							href="${fn:escapeXml(removeQueryUrl)}"><span --%>
<!-- 								class="float-right glyphicon glyphicon-remove text-prussianblue"></span></a></li> -->
<%-- 					</c:forEach> --%>
<!-- 				</ul> -->
<!-- 			</div> -->
<!-- 			<!-- card-body.// -->
<!-- 		</div> -->
<!-- 		<!-- collapse .// -->
<!-- 	</article> -->

</c:if>