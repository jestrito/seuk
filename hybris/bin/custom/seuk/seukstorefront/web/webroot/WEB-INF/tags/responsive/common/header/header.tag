<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url
	value="https://www.socialenterprise.org.uk/wp-content/uploads/2019/02/SEUKLogoPNGblack.png"
	var="seukBlackLogo" />

<c:url value="https://www.socialenterprise.org.uk/media-centre/"
	var="seukMediaCentreLink" />
<c:url
	value="https://directdebit.socialenterprise.org.uk/preferences.aspx"
	var="seukStayInTouchLink" />
<c:url value="https://directdebit.socialenterprise.org.uk/Signup.aspx"
	var="seukJoinUsLink" />
<c:url value="https://www.socialenterprise.org.uk/login"
	var="seukMembersLoginLink" />

<cms:pageSlot position="TopHeaderSlot" var="component" element="div">
	<cms:component component="${component}" />
</cms:pageSlot>

<header class="stick">
	<div class="container">
		<div style="max-width: 1240px; width: 100%; padding-left: 20px; padding-right: 20px; margin: 0 auto; display: block;">
			<a href="https://www.socialenterprise.org.uk"> <img
				class="mainLogo" src="${seukBlackLogo}" title="Home" />
			</a>
			<div class="mainCallToAction">
				<ul class="socialIcons">
					<li><a class="twitter" href="https://twitter.com/SocialEnt_UK"
						target="_blank"
						style="background-color: #55acee; margin-left: 5px;">Twitter</a></li>
					<li><a class="facebook"
						href="https://www.facebook.com/Social-Enterprise-UK-136948359726274/"
						target="_blank" style="background-color: #3b5998;">Facebook</a></li>
					<li><a class="flickr"
						href="https://www.flickr.com/photos/social_enterprise_coalition/albums"
						target="_blank" style="background-color: #0063dc;">Flickr</a></li>
					<li><a class="linkedin" href="https://www.linkedin.com/"
						target="_blank" style="background-color: #0077b5;">LinkedIn</a></li>
					<li><a class="fa-instagram"
						href="https://www.instagram.com/socialenterpriseuk/"
						target="_blank" style="background-color: #3f729b;">Instagram</a></li>
				</ul>
				<div class="media_btn button"
					style="background: #000000; border: 2px solid #000000;">
					<a href="${seukMediaCentreLink}" target="_self"> <span
						class="post_link">Media Centre</span>
					</a>
				</div>
				<div class="media_btn button"
					style="background: #00a3ad; border: 2px solid #00a3ad;">
					<a href="${seukStayInTouchLink}" target="_self"> <span
						class="post_link">Stay in Touch</span>
					</a>
				</div>
				<div class="media_btn button"
					style="background: #0085ca; border: 2px solid #0085ca;">
					<a href="${seukJoinUsLink}" target="_self"> <span
						class="post_link">Join Us</span>
					</a>
				</div>
				<div class="media_btn button"
					style="background: #ec008b; border: 2px solid #ec008b;">
					<a href="${seukMembersLoginLink}" target="_self"> <span
						class="post_link">Members Area Log In</span>
					</a>
				</div>
			</div>
		</div>
		<nav class="menuMain">
			<div class="menuMain">
				<ul class="topLevel level1 horizontal-center">
					<li><a>About Us</a>
						<ul></ul></li>
					<li><a>Join Us</a>
						<ul></ul></li>
					<li><a>What We Do</a>
						<ul></ul></li>
					<li><a>Who We Work With</a>
						<ul></ul></li>
					<li><a>Local Work</a>
						<ul></ul></li>
					<li><a>News & Events</a>
						<ul></ul></li>
					<li><a>Advice & Resources</a>
						<ul></ul></li>
					<li><a>About Social Enterprise</a>
						<ul></ul></li>
				</ul>
			</div>
		</nav>
	</div>
	<!-- 	<a id="skiptonavigation"></a> -->
	<nav:topNavigation />
</header>
<%-- <nav:topNavigationResponsive /> --%>

<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"
	class="container-fluid">
	<cms:component component="${component}" />
</cms:pageSlot>
