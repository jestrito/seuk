# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
# ImpEx for Importing CMS Content
# English Localisation

# Macros / Replacement Parameter definitions
$contentCatalog=seukContentCatalog

$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]
$picture=media(code, $contentCV);
$siteResource=jar:org.seuk.initialdata.setup.InitialDataSystemSetup&/seukinitialdata/import/contentCatalogs/$contentCatalog

# Language
$lang=en

# Site Logo Component
INSERT_UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]
;;SiteLogoComponent;/images/theme/logo-hybris.png

# CMS Link Components
INSERT_UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];linkName[lang=$lang]
;;TradingActivityCategoryLink;"Trading Activity"
;;LocationCategoryLink;"Location"
;;SocialEnvCategoryLink;"Social/Environmental Mission"

# Trading Activity
# Trading Activity Main Menu
;;AgriFarmMainCatLink;"Agriculture – farming/gardening"
# Trading Activity Sub Menu
;;AgriProdCategoryLink;"Agricultural production (crops/livestock)"
;;GardeningCategoryLink;"Gardening/landscaping"

# Trading Activity Main Menu
;;CommAndGenAervMainCatLink;"Community and general services"
# Trading Activity Sub Menu
;;ChildCareCategoryLink;"Child care"
;;DomesticCleanServCategoryLink;"Domestic/cleaning services"
;;EnvironmentRecycleCategoryLink;"Environmental – recycling, re-use etc."
;;HairBeautyCategoryLink;"Hair/Beauty/Spa/Massage"
;;HousingCategoryLink;"Housing"
;;RepairsCategoryLink;"Repairs (auto, plumbing, carpentry etc.)"
;;SportCategoryLink;"Sport and leisure services"
;;TelecommunicationsCategoryLink;"Telecommunications"
;;TransportCategoryLink;"Transport"

# Trading Activity Main Menu
;;CreativeIndustriesMainCatLink;"Creative industries"
# Trading Activity Sub Menu
;;AdvertisingCategoryLink;"Advertising and marketing"
;;ArchitectureCategoryLink;"Architecture"
;;CraftsCategoryLink;"Crafts"
;;DesignCategoryLink;"Design: product, graphic and fashion design"
;;FilmCategoryLink;"Film, TV, video, radio and photography"
;;PrintingCategoryLink;"Printing and publishing"
;;MuseumsCategoryLink;"Museums, galleries and libraries"
;;MusicCategoryLink;"Music, performing and visual arts"

# Trading Activity Main Menu
;;EducationSkillDevMainCatLink;"Education and skills development"
# Trading Activity Sub Menu
;;CareerCoachingCategoryLink;"Career coaching and employment support"
;;UniversityCategoryLink;"College/University"
;;MentoringCategoryLink;"Mentoring"
;;TutoringServicesCategoryLink;"Tutoring services"
;;VocationalCategoryLink;"Vocational/technical training"

# Trading Activity Main Menu
;;HealthSocialCareMainCatLink;"Health And Social Care"

# Trading Activity Main Menu
;;HealthCareMainCatLink;"Health Care"
# Trading Activity Sub Menu
;;CommunityServicesCategoryLink;"Community services"
;;DentistryCategoryLink;"Dentistry"
;;MentalhealthCategoryLink;"Mental health services (including psychological therapies)"
;;OutOfHoursCategoryLink;"Out of hours services"
;;PrimaryCareCategoryLink;"Primary Care"
;;PrisonHealthCategoryLink;"Prison Health"
;;PublicHealthLocalCategoryLink;"Public Health Services (local authority commissioned)"
;;PublicHealthCategoryLink;"Public Health Services (NHS commissioned)"
;;TherapyServicesCategoryLink;"Therapy services (physiotherapy, OT, dietetics, speech and language, podiatry)"

# Trading Activity Main Menu
;;SocialCareMainCatLink;"Social Care"
# Trading Activity Sub Menu
;;AdultCategoryLink;"Adult social care"
;;ChildrenCategoryLink;"Children and young people's services"
;;DomiciliaryCategoryLink;"Domiciliary care"
;;MentalHealthServCategoryLink;"Mental health services"
;;ResidentialCareCategoryLink;"Residential care"
;;ServicesForOPCategoryLink;"Services for older people"
;;ServicesForPLDCategoryLink;"services for people with learning disabilities"
;;ServicesForPWDCategoryLink;"services for people with physical disabilities"
;;SubstanceAbuseServiceCategoryLink;"Substance abuse services"

# Trading Activity Main Menu
;;HospAndFoodMainCatLink;"Hospitality and food"
# Trading Activity Sub Menu
;;AccommodationCategoryLink;"accommodation"
;;BarCategoryLink;"bar"
;;BeverageAlcoholicCategoryLink;"beverage manufacturing/supply (alcoholic)"
;;BeverageNonAlcoholicCategoryLink;"beverage manufacturing/supply (non-alcoholic)"
;;CafeCategoryLink;"Café/bakery/coffee shop"
;;CateringServiceCategoryLink;"Catering services"
;;foodProductionCategoryLink;"food production"
;;GroceryStoreCategoryLink;"Grocery/convenience store"
;;RestaurantCategoryLink;"Restaurant"
;;VenueHireCategoryLink;"Venue hire"

# Trading Activity Main Menu
;;ManufacturingMainCatLink;"Manufacturing"
# Trading Activity Sub Menu - NO SUB CATEGORY

# Trading Activity Main Menu
;;ProfessionalServMainCatLink;"Professional Services"
# Trading Activity Sub Menu
;;BusinessTransportCategoryLink;"Business transport and logistics"
;;EventPlanningCategoryLink;"Event planning"
;;FinancialServiceCategoryLink;"Financial services"
;;LegalServiceCategoryLink;"Legal services"
;;ManagementConsultingCategoryLink;"Management consulting/business support"
;;MarketingCategoryLink;"Marketing/PR/advertising"
;;TechnologyServiceCategoryLink;"Technology services"

# Trading Activity Main Menu
;;RealEstateMainCatLink;"Real estate/storage"
# Trading Activity Sub Menu
;;EstateAgentCategoryLink;"Estate agents"
;;RealEstateHousingCategoryLink;"Housing"
;;HousingAssociationsCategoryLink;"Housing associations"
;;StorageCategoryLink;"Storage"
;;WorkspaceHireCategoryLink;"Workspace/room hire"

# Trading Activity Main Menu
;;RetailMainCatLink;"Retail"
# Trading Activity Sub Menu
;;ClothingCategoryLink;"Clothing"
;;ElectronicsCategoryLink;"Electronics"
;;GiftsCategoryLink;"Gifts"
;;HobbiesAndLeisureCategoryLink;"hobbies and leisure products"
;;HomeAndGardenCategoryLink;"Home and garden"
;;OfficeSuppliesCategoryLink;"office supplies"
;;SoftwareCategoryLink;"Software"

# Social/Environment Mission
;;CommBuildingOppCategoryLink;"Community building and opportunities"
;;EducLiteracyCategoryLink;"Education and literacy"
;;ElderlyCareCategoryLink;"Elderly care"
;;EmploymentOpportunitiesCategoryLink;"Employment opportunities"
;;EnvironmentCategoryLink;"Environment"
;;FinancialExclusionCategoryLink;"Financial exclusion"
;;GenderEqualityCategoryLink;"Gender equality"
;;HomelessnessAndHousingCategoryLink;"Homelessness and housing"
;;MentalHealthAndWellbeingCategoryLink;"Mental health and wellbeing"
;;PhysicalHealthAndWellbeingCategoryLink;"Physical health and wellbeing"
;;PovertyCategoryLink;"Poverty"
;;SocialInclusionCategoryLink;"Social inclusion"
;;SupportingEnterprisesCategoryLink;"Supporting other social ent/orgs"
;;YouthDevelopmentCategoryLink;"Youth development and opportunities"

# Location
# Location Main Menu
;;ScotLandCategoryLink;"Scotland"
# Location Sub Menu
;;AberdeenshireSubCategoryLink;"Aberdeenshire"
;;AngusSubCategoryLink;"Angus"
;;ArgyllAndButeSubCategoryLink;"Argyll & Bute"
;;AyrshireSubCategoryLink;"Ayrshire"
;;BanffshireSubCategoryLink;"Banffshire"
;;BerwickshireSubCategoryLink;"Berwickshire"
;;BordersSubCategoryLink;"Borders"
;;CaithnessSubCategoryLink;"Caithness"
;;ClackmannanshireSubCategoryLink;"Clackmannanshire"
;;DumfriesAndGallowaySubCategoryLink;"Dumfries & Galloway"
;;DunbartonshireSubCategoryLink;"Dunbartonshire"
;;EastAyrshireSubCategoryLink;"East Ayrshire"
;;EastDunbartonshireSubCategoryLink;"East Dunbartonshire"
;;EastLothianSubCategoryLink;"East Lothian"
;;EastRenfrewshireSubCategoryLink;"East Renfrewshire"
;;FifeSubCategoryLink;"Fife"
;;HighlandSubCategoryLink;"Highland"
;;InverclydeSubCategoryLink;"Inverclyde"
;;KincardineshireSubCategoryLink;"Kincardineshire"
;;LanarkshireSubCategoryLink;"Lanarkshire"
;;MidlothianSubCategoryLink;"Midlothian"
;;MoraySubCategoryLink;"Moray"
;;NorthAyrshireSubCategoryLink;"North Ayrshire"
;;NorthLanarkshireSubCategoryLink;"North Lanarkshire"
;;OrkneySubCategoryLink;"Orkney"
;;PerthAndKinrossSubCategoryLink;"Perth & Kinross"
;;RenfrewshireSubCategoryLink;"Renfrewshire"
;;ShetlandSubCategoryLink;"Shetland"
;;SouthAyrshireSubCategoryLink;"South Ayrshire"
;;SouthLanarkshireSubCategoryLink;"South Lanarkshire"
;;StirlingshireSubCategoryLink;"Stirlingshire"
;;WestDunbartonshireSubCategoryLink;"West Dunbartonshire"
;;WestLothianSubCategoryLink;"West Lothian"
;;WesternIslesSubCategoryLink;"Western Isles"

# Location Main Menu
;;NorthernIrelandCategoryLink;"Northern Ireland"
# Location Sub Menu
;;AntrimSubCategoryLink;"Antrim"
;;ArmaghSubCategoryLink;"Armagh"
;;DownSubCategoryLink;"Down"
;;FermanaghSubCategoryLink;"Fermanagh"
;;LondonderrySubCategoryLink;"Londonderry"
;;TyroneSubCategoryLink;"Tyrone"

# Location Main Menu
;;WalesCategoryLink;"Wales"
# Location Sub Menu
;;BlaenauGwentSubCategoryLink;"Blaenau Gwent"
;;BridgendSubCategoryLink;"Bridgend"
;;CaerphillySubCategoryLink;"Caerphilly"
;;CardiffSubCategoryLink;"Cardiff"
;;CarmarthenshireSubCategoryLink;"Carmarthenshire"
;;CeredigionSubCategoryLink;"Ceredigion"
;;ConwySubCategoryLink;"Conwy"
;;DenbighshireSubCategoryLink;"Denbighshire"
;;FlintshireSubCategoryLink;"Flintshire"
;;GwyneddSubCategoryLink;"Gwynedd"
;;IsleOfAngleseySubCategoryLink;"Isle of Anglesey"
;;MerthyrTydfilSubCategoryLink;"Merthyr Tydfil"
;;MonmouthshireSubCategoryLink;"Monmouthshire"
;;NeathPortTalbotSubCategoryLink;"Neath Port Talbot"
;;NewportSubCategoryLink;"Newport"
;;PembrokeshireSubCategoryLink;"Pembrokeshire"
;;PowysSubCategoryLink;"Powys"
;;RhonddaCynonTaffSubCategoryLink;"Rhondda Cynon Taff"
;;SwanseaSubCategoryLink;"Swansea"
;;TorfaenSubCategoryLink;"Torfaen"
;;ValeOffGlamorganSubCategoryLink;"Vale of Glamorgan"
;;WrexhamSubCategoryLink;"Wrexham"

# Location Main Menu
;;NorthEastEnglandCategoryLink;"North East England"
# Location Sub Menu
;;CountyDurhamSubCategoryLink;"County Durham"
;;NorthumberlandSubCategoryLink;"Northumberland"
;;TyneAndWearSubCategoryLink;"Tyne and Wear"

# Location Main Menu
;;NorthWestEnglandCategoryLink;"North West England"
# Location Sub Menu
;;CumbriaSubCategoryLink;"Cumbria"
;;GreaterManchesterSubCategoryLink;"Greater Manchester"
;;LancashireSubCategoryLink;"Lancashire"
;;MerseysideSubCategoryLink;"Merseyside"

# Location Main Menu
;;YorkshireAndHumberCategoryLink;"Yorkshire & Humber"
# Location Sub Menu
;;EastRidingOfYorkshireSubCategoryLink;"East Riding of Yorkshire"
;;LincolnshireSubCategoryLink;"Lincolnshire"
;;NorthYorkshireSubCategoryLink;"North Yorkshire"
;;SouthYorkshireSubCategoryLink;"South Yorkshire"
;;WestYorkshireSubCategoryLink;"West Yorkshire"

# Location Main Menu
;;WestMidlandsCategoryLink;"West Midlands"
# Location Sub Menu
;;CheshireSubCategoryLink;"Cheshire"
;;DerbyshireSubCategoryLink;"Derbyshire"
;;HerefordshireSubCategoryLink;"Herefordshire"
;;ShropshireSubCategoryLink;"Shropshire"
;;StaffordshireSubCategoryLink;"Staffordshire"
;;WarwickshireSubCategoryLink;"Warwickshire"
;;WestMidlandsSubCategoryLink;"West Midlands"
;;WorcestershireSubCategoryLink;"Worcestershire"

# Location Main Menu
;;EastMidlandsCategoryLink;"East Midlands"
# Location Sub Menu
;;LeicestershireSubCategoryLink;"Leicestershire"
;;NorthamptonshireSubCategoryLink;"Northamptonshire"
;;NottinghamshireSubCategoryLink;"Nottinghamshire"
;;RutlandSubCategoryLink;"Rutland"

# Location Main Menu
;;SouthWestCategoryLink;"South West England"
# Location Sub Menu
;;BristolSubCategoryLink;"Bristol"
;;CornwallSubCategoryLink;"Cornwall"
;;DevonSubCategoryLink;"Devon"
;;DorsetSubCategoryLink;"Dorset"
;;GloucestershireSubCategoryLink;"Gloucestershire"
;;SomersetSubCategoryLink;"Somerset"
;;WiltshireSubCategoryLink;"Wiltshire"

# Location Main Menu
;;SouthEastCategoryLink;"South East England"
# Location Sub Menu
;;BerkshireSubCategoryLink;"Berkshire"
;;BuckinghamshireSubCategoryLink;"Buckinghamshire"
;;EastSussexSubCategoryLink;"East Sussex"
;;HampshireSubCategoryLink;"Hampshire"
;;IsleOfWightSubCategoryLink;"Isle of Wight"
;;KentSubCategoryLink;"Kent"
;;OxfordshireSubCategoryLink;"Oxfordshire"
;;SurreySubCategoryLink;"Surrey"
;;WestSussexSubCategoryLink;"West Sussex"

# Location Main Menu
;;EastEnglandCategoryLink;"East England"
# Location Sub Menu
;;BedfordshireSubCategoryLink;"Bedfordshire"
;;CambridgeshireSubCategoryLink;"Cambridgeshire"
;;EssexSubCategoryLink;"Essex"
;;HertfordshireSubCategoryLink;"Hertfordshire"
;;NorfolkSubCategoryLink;"Norfolk"
;;SuffolkSubCategoryLink;"Suffolk"

# Location Main Menu
;;LondonCategoryLink;"London"
# Location Sub Menu
;;GreaterLondonSubCategoryLink;"Greater London"

;;AboutSAPCommerceLink;"About SAP Commerce Cloud"
;;AccountAddressBookLink;"Address Book"
;;AccountHomeLink;"My Account"
;;AccountManageAddressLink;"Manage your delivery addresses"
;;AccountManagePaymentDetailsLink;"Manage your payment details"
;;AccountOrderHistoryLink;"Order History"
;;AccountPaymentDetailsLink;"Payment Details"
;;AccountPersonalDetailsLink;"Update personal details"
;;AccountProfileLink;"Profile"
;;AccountUpdatePasswordLink;"Change your password"
;;AccountViewOrderHistoryLink;"View order history"
;;AgileCommerceBlogLink;"Agile Commerce Blog"
;;AllBrandsCategoryLink;"Trading Activity"
;;CameraAccessoriesCategoryLink;"Location"
;;CamerasCategoryLink;"Cameras"
;;CloseAccountLink;"Close Account"
;;ComponentsCategoryLink;"Components"
;;ConsentManagementLink;"Consent Management"
;;ContactUsLink;"Contact Us"
;;DigitalCamerasCategoryLink;"Social/Environmental Mission"
;;DocumentationLink;"Documentation"
;;FAQLink;"FAQ"
;;FacebookLink;"Facebook"
;;FilmCamerasCategoryLink;"Film Cameras"
;;FlashMemoryCategoryLink;"Flash Memory"
;;HandheldCamcordersCategoryLink;"Hand Held Camcorders"
;;HomepageNavLink;"Home"
;;LinkedInLink;"Linked In"
;;PowerSuppliesCategoryLink;"Power Supplies"
;;SpecialOffersLink;"Special Offers"
;;TwitterLink;"Twitter"
;;VisitSAPLink;"Visit SAP"

# Lightbox Banner for Mini Cart (banner is not localizable so we must create a separate banner per language)
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];&imageRef;folder(qualifier)[default='images'];altText
;;Elec_358x45_HomeDeliveryBanner_EN_01.gif;$siteResource/images/banners/site/Elec_358x45_HomeDeliveryBanner_EN_01.gif;;Elec_358x45_HomeDeliveryBanner_EN_01.gif;;"Order before 6pm for next day delivery"

INSERT_UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]
;;LightboxHomeDeliveryBannerComponent;Elec_358x45_HomeDeliveryBanner_EN_01.gif

# CMS Mini Cart Component
INSERT_UPDATE MiniCartComponent;$contentCV[unique=true];uid[unique=true];name;title[lang=$lang]
;;MiniCart;Mini Cart;"YOUR SHOPPING CART"

# CMS Tab Paragraph Components
INSERT_UPDATE CMSTabParagraphComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang];content[lang=$lang]
;;deliveryTab;Delivery;"<div class=""tab-delivery"">Lorem ipsum dolor sit amet, dolor sed, ut nam ut. Senectus mauris egestas a massa, enim placeat wisi congue purus fermentum. Ut aptent mauris dapibus congue in sit. Sed dolor varius amet feugiat volutpat dignissim, pede a rhoncus sodales aliquam adipiscing, dapibus massa fusce. Dui egestas ornare urna nibh facilisi, cras posuere. Lorem aliquam accumsan eleifend sem libero lorem, aliquam sequi sed urna nec. Eget dolor quisque dolor, amet suspendisse ullamcorper minus elit lectus nunc, est mattis dui id eu et facilisis, conubia sit tristique. Ac fusce gravida condimentum iaculis neque, a platea curabitur accumsan porttitor vel justo. Amet potenti ac, eget amet ducimus sit nulla, ac porttitor rhoncus, justo proin tortor integer turpis nulla vitae. Egestas mollis litora nunc platea dui, eu semper mauris diam, erat quam, porta maecenas fusce libero non aliquet. Amet tellus taciti ligula sed sollicitudin, nonummy cursus enim, hendrerit nec, sed lacus sed at sit quis, semper a arcu mollis sapien nec pretium. Ante mauris eros nec, nonummy mauris, nulla lacinia vel. Volutpat luctus velit eu.</div>"

# CMS Footer Component
INSERT_UPDATE FooterComponent;$contentCV[unique=true];uid[unique=true];notice[lang=$lang]
;;FooterComponent;"Copyright © {0} SAP SE or an SAP affiliate company. All rights reserved."

# SEUK-42
# CMS Paragraph Component (Contact information)
UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en];
;;HomepageWhoWeAreComponent;"<h2>Who we are</h2><span>We are the national body for social enterprise – business with a social or environmental mission.</span><p>Proin luctus, justo sit amet laoreet venenatis, libero velit tincidunt mi, nec fermentum ante massa id quam. In gravida erat vel diam blandit consequat morbi. Ut interdum nuceu egestas arcu uspend isse sodales. Eiusmod tempor incidiunt labore velit dolore magna aliqu sed enimi nim.</p><p><a href='https://www.socialenterprise.org.uk/who-we-are/' target='_self'>Read More</a></p>";
# SEUK-42

# CMS Paragraph Components
INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang]

# CMS Product References Components
INSERT_UPDATE ProductReferencesComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;CrossSelling;"You may also like..."
;;Others;"Accessories"
;;Similar;"You may also like..."
;;accessories;"Accessories"

# CMS PurchasedCategorySuggestionComponent Components
INSERT_UPDATE PurchasedCategorySuggestionComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;PurchasedCategorySuggestions;"You may also like..."

# CMS CartSuggestion Components
INSERT_UPDATE CartSuggestionComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;CartSuggestions;"You may also like..."

# Category Pages
INSERT_UPDATE CategoryPage;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;Cameras;"Cameras"
;;DigitalCameras;"Digital Cameras"

# CMS Navigation Nodes
INSERT_UPDATE CMSNavigationNode;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;TradingActivityNavNode;"Trading Activity"
;;LocationNavNode;"Location"
;;SocialEnvMissionNavNode;"Social/Environmental Mission"
;;FLTradeAgricultureNavNode;"Agriculture – farming/gardening"
;;FLTradeCommunityAndGenServNavNode;"Community and general services"
;;FLTradeCreativeIndustriesNavNode;"Creative industries"
;;FLTradeEducationAndSkillsDevNavNode;"Education and skills development"
;;FLTradeHealthAndSocialCareNavNode;"Health and Social Care"
;;FLTradeHospitalityAndFoodNavNode;"Hospitality and food"
;;FLTradeManufacturingNavNode;"Manufacturing"
;;FLTradeProfessionalServNavNode;"Professional Services"
;;FLTradeRealEstateNavNode;"Real estate/storage"
;;FLTradeRetailNavNode;"Retail"
;;SLTradeAgriProductionNavNode;"Agricultural production (crops/livestock)"
;;SLTradeGardeningNavNode;"Gardening/landscaping"
;;SLTradeChildCareNavNode;"Child care"
;;SLTradeDomesticServNavNode;"Domestic/cleaning services"
;;SLTradeEnvironmentalNavNode;"Environmental – recycling, re-use etc."
;;SLTradeHairNavNode;"Hair/Beauty/Spa/Massage"
;;SLTradeHousingNavNode;"Housing "
;;SLTradeRepairsNavNode;"Repairs (auto, plumbing, carpentry etc.)"
;;SLTradeSportServNavNode;"Sport and leisure services"
;;SLTradeTelecommNavNode;"Telecommunications"
;;SLTradeTransportNavNode;"Transport"
;;SLTradeTradeAdvertisingNavNode;"Advertising and marketing"
;;SLTradeArchitectureNavNode;"Architecture"
;;SLTradeCraftsNavNode;"Crafts"
;;SLTradeDesignNavNode;"Design: product, graphic and fashion design"
;;SLTradeFilmNavNode;"Film, TV, video, radio and photography"
;;SLTradePrintingNavNode;"Printing and publishing"
;;SLTradeMuseumsNavNode;"Museums, galleries and libraries"
;;SLTradeMusicNode;"Music, performing and visual arts"
;;SLTradeCareerCoachNavNode;"Career coaching and employment support"
;;SLTradeCollegeNavNode;"College/University"
;;SLTradeMentoringNavNode;"Mentoring"
;;SLTradeTutoringServNavNode;"Tutoring services"
;;SLTradeVocationalNavNode;"Vocational/technical training"

;;SLTradeHealthCareNavNode;"Health Care"
;;SLTradeSocialcareNavNode;"Social care"

;;TLTradeCommServNavNode;"Community services"
;;TLTradeDentistryNavNode;"Dentistry"
;;TLTradeMentalHealthServNavNode;"Mental health services (including psychological therapies)"
;;TLTradeOutOfHourServNavNode;"Out of hours services"
;;TLTradePrimaryCareNavNode;"Primary Care"
;;TLTradePrisonHealthNavNode;"Prison Health"
;;TLTradePublicHealthServLocalNavNode;"Public Health Services (local authority commissioned)"
;;TLTradePublicHealthServNHSNavNode;"Public Health Services (NHS commissioned)"
;;TLTradeTherapyServNavNode;"Therapy services (physiotherapy, OT, dietetics, speech and language, podiatry)"
;;TLTradeAdultSocialCareNavNode;"Adult social care"
;;TLTradeChildrenAndYoungServNavNode;"Children and young people services"
;;TLTradeDomiciliaryCareNavNode;"Domiciliary care"
;;TLTradeMentalHealthServNavNode;"Mental health services"
;;TLTradeResidentialCareNavNode;"Residential care"
;;TLTradeServForOldPeopleNavNode;"Services for older people"
;;TLTradeServForPLDNavNode;"services for people with learning disabilities"
;;TLTradeServForPPDNavNode;"services for people with physical disabilities"
;;TLTradeSubstanceAbuseServNavNode;"Substance abuse services"
;;SLTradeAccommodationNavNode;"accommodation"
;;SLTradeBarNavNode;"bar"
;;SLTradeBeverageAlcoholicNavNode;"beverage manufacturing/supply (alcoholic)"
;;SLTradebeverageNonAlcoholicNavNode;"beverage manufacturing/supply (non-alcoholic)"
;;SLTradeCafeShopNavNode;"Café/bakery/coffee shop"
;;SLTradeCateringServNavNode;"Catering services"
;;SLTradeFoodProdNavNode;"food production"
;;SLTradeGroceryStoreNavNode;"Grocery/convenience store"
;;SLTradeRestaurantNavNode;"Restaurant"
;;SLTradeVenueHireNavNode;"Venue hire"
;;SLTradeBusinessTransportNavNode;"Business transport and logistics"
;;SLTradeEventPlanningNavNode;"Event planning"
;;SLTradeFinancialServNavNode;"Financial services"
;;SLTradeLegalServNavNode;"Legal services"
;;SLTradeManagementConsultingNavNode;"Management consulting/business support"
;;SLTradeMarketingNavNode;"Marketing/PR/advertising"
;;SLTradeTechnologyServNavNode;"Technology services"
;;SLTradeEstateAgentsNavNode;"Estate agents"
;;SLTradeHousingNavNode;"Housing"
;;SLTradeHousingAssociationsNavNode;"Housing associations"
;;SLTradeStorageNavNode;"Storage"
;;SLTradeWorkspaceNode;"Workspace/room hire"
;;SLTradeClothingNavNode;"Clothing"
;;SLTradeElectronicsNavNode;"electronics"
;;SLTradeGiftsNavNode;"gifts"
;;SLTradeNobbiesNavNode;"hobbies and leisure products"
;;SLTradeHomeAndGardenNavNode;"Home and garden"
;;SLTradeOfficeSuppliesNavNode;"office supplies"
;;SLTradeSoftwareNavNode;"software"
;;FLLocEastEnglandNavNode;"East England"
;;FLLocEastMidlandsNavNode;"East Midlands"
;;FLLocLondonNavNode;"London"
;;FLLocNorthEastEnglandNavNode;"North East England"
;;FLLocNorthWestEnglandNavNode;"North West England"
;;FLLocNorthernIrelandNavNode;"Northern Ireland"
;;FLLocScotlandNavNode;"Scotland"
;;FLLocSouthEastEnglandNavNode;"South East England"
;;FLLocSouthWestEnglandNavNode;"South West England"
;;FLLocWalesNavNode;"Wales"
;;FLLocWestMidlandsNavNode;"West Midlands"
;;FLLocYorkshireAndHumberNavNode;"Yorkshire & Humber"
;;SLLocBedfordshireNavNode;"Bedfordshire"
;;SLLocCambridgeshireNavNode;"Cambridgeshire"
;;SLLocEssexNavNode;"Essex"
;;SLLocHertfordshireNavNode;"Hertfordshire"
;;SLLocNorfolkNavNode;"Norfolk"
;;SLLocSuffolkNavNode;"Suffolk"
;;SLLocLeicestershireNavNode;"Leicestershire"
;;SLLocNorthamptonshireNavNode;"Northamptonshire"
;;SLLocNottinghamshireNavNode;"Nottinghamshire"
;;SLLocRutlandNavNode;"Rutland"
;;SLLocGreaterLondonNavNode;"Greater London"
;;SLLocCountyDurhamNavNode;"County Durham"
;;SLLocNorthumberlandNavNode;"Northumberland"
;;SLLocTyneAndWearNavNode;"Tyne and Wear"
;;SLLocCumbriaNavNode;"Cumbria"
;;SLLocGreaterManchesterNavNode;"Greater Manchester"
;;SLLocLancashireNavNode;"Lancashire"
;;SLLocMerseysideNavNode;"Merseyside"
;;SLLocAntrimNavNode;"Antrim"
;;SLLocArmaghNavNode;"Armagh"
;;SLLocDownNavNode;"Down"
;;SLLocFermanaghNavNode;"Fermanagh"
;;SLLocLondonderryNavNode;"Londonderry"
;;SLLocTyroneNavNode;"Tyrone"
;;SLLocAberdeenshireNavNode;"Aberdeenshire"
;;SLLocAngusNavNode;"Angus"
;;SLLocArgyllAndButeNavNode;"Argyll & Bute"
;;SLLocAyrshireNavNode;"Ayrshire"
;;SLLocBanffshireNavNode;"Banffshire"
;;SLLocBerwickshireNavNode;"Berwickshire"
;;SLLocBordersNavNode;"Borders"
;;SLLocCaithnessNavNode;"Caithness"
;;SLLocClackmannanshireNavNode;"Clackmannanshire"
;;SLLocDumfriesAndGallowayNavNode;"Dumfries & Galloway"
;;SLLocDunbartonshireNavNode;"Dunbartonshire"
;;SLLocEastAyrshireNavNode;"East Ayrshire"
;;SLLocEastDunbartonshireNavNode;"East Dunbartonshire"
;;SLLocEastLothianNavNode;"East Lothian"
;;SLLocEastRenfrewshireNavNode;"East Renfrewshire"
;;SLLocFifeNavNode;"Fife"
;;SLLocHighlandNavNode;"Highland"
;;SLLocInverclydeNavNode;"Inverclyde"
;;SLLocKincardineshireNavNode;"Kincardineshire"
;;SLLocLanarkshireNavNode;"Lanarkshire"
;;SLLocMidlothianNavNode;"Midlothian"
;;SLLocMorayNavNode;"Moray"
;;SLLocNorthAyrshireNavNode;"North Ayrshire"
;;SLLocNorthLanarkshireNavNode;"North Lanarkshire"
;;SLLocOrkneyNavNode;"Orkney"
;;SLLocPerthAndKinrossNavNode;"Perth & Kinross"
;;SLLocRenfrewshireNavNode;"Renfrewshire"
;;SLLocShetlandNavNode;"Shetland"
;;SLLocSouthAyrshireNavNode;"South Ayrshire"
;;SLLocSouthLanarkshireNavNode;"South Lanarkshire"
;;SLLocStirlingshireNavNode;"Stirlingshire"
;;SLLocWestDunbartonshireNavNode;"West Dunbartonshire"
;;SLLocWestLothianNavNode;"West Lothian"
;;SLLocWesternIslesNavNode;"Western Isles"
;;SLLocBerkshireNavNode;"Berkshire"
;;SLLocBuckinghamshireNavNode;"Buckinghamshire"
;;SLLocEastSussexNavNode;"East Sussex"
;;SLLocHampshireNavNode;"Hampshire"
;;SLLocIsleOfWightNavNode;"Isle of Wight"
;;SLLocKentNavNode;"Kent"
;;SLLocOxfordshireNavNode;"Oxfordshire"
;;SLLocSurreyNavNode;"Surrey"
;;SLLocWestSussexNavNode;"West Sussex"
;;SLLocBristolNavNode;"Bristol"
;;SLLocCornwallNavNode;"Cornwall"
;;SLLocDevonNavNode;"Devon"
;;SLLocDorsetNavNode;"Dorset"
;;SLLocGloucestershireNavNode;"Gloucestershire"
;;SLLocSomersetNavNode;"Somerset"
;;SLLocWiltshireNavNode;"Wiltshire"
;;SLLocBlaenauGwentNavNode;"Blaenau Gwent"
;;SLLocBridgendNavNode;"Bridgend"
;;SLLocCaerphillyNavNode;"Caerphilly"
;;SLLocCardiffNavNode;"Cardiff"
;;SLLocCarmarthenshireNavNode;"Carmarthenshire"
;;SLLocCeredigionNavNode;"Ceredigion"
;;SLLocConwyNavNode;"Conwy"
;;SLLocDenbighshireNavNode;"Denbighshire"
;;SLLocFlintshireNavNode;"Flintshire"
;;SLLocGwyneddNavNode;"Gwynedd"
;;SLLocIsleOfAngleseyNavNode;"Isle of Anglesey"
;;SLLocMerthyrTydfilNavNode;"Merthyr Tydfil"
;;SLLocMonmouthshireNavNode;"Monmouthshire"
;;SLLocNeathPortTalbotNavNode;"Neath Port Talbot"
;;SLLocNewportNavNode;"Newport"
;;SLLocPembrokeshireNavNode;"Pembrokeshire"
;;SLLocPowysNavNode;"Powys"
;;SLLocRhonddaCynonTaffNavNode;"Rhondda Cynon Taff"
;;SLLocSwanseaNavNode;"Swansea"
;;SLLocTorfaenNavNode;"Torfaen"
;;SLLocValeOfGlamorganNavNode;"Vale of Glamorgan"
;;SLLocWrexhamNavNode;"Wrexham"
;;SLLocCheshireNavNode;"Cheshire"
;;SLLocDerbyshireNavNode;"Derbyshire"
;;SLLocHerefordshireNavNode;"Herefordshire"
;;SLLocShropshireNavNode;"Shropshire"
;;SLLocStaffordshireNavNode;"Staffordshire"
;;SLLocWarwickshireNavNode;"Warwickshire"
;;SLLocWestMidlandsNavNode;"West Midlands"
;;SLLocWorcestershireNavNode;"Worcestershire"
;;SLLocEastRidingOfYorkshireNavNode;"East Riding of Yorkshire"
;;SLLocLincolnshireNavNode;"Lincolnshire"
;;SLLocNorthYorkshireNavNode;"North Yorkshire"
;;SLLocSouthYorkshireNavNode;"South Yorkshire"
;;SLLocWestYorkshireNavNode;"West Yorkshire"
;;FLMissionCommBuildAndOppNavNode;"Community building and opportunities"
;;FLMissionEducAndLitNavNode;"Education and literacy"
;;FLMissionElderlyCareNavNode;"Elderly care"
;;FLMissionEmploymentOppNavNode;"Employment opportunities"
;;FLMissionEnvironmentNavNode;"Environment"
;;FLMissionFinancialExclusionNavNode;"Financial exclusion"
;;FLMissionGenderEqualityNavNode;"Gender equality"
;;FLMissionHomelessnessAndHousingNavNode;"Homelessness and housing"
;;FLMissionMentalHealthAndWellbeingNavNode;"Mental health and wellbeing"
;;FLMissionPhysicalHealthAndWellbeingNavNode;"Physical health and wellbeing"
;;FLMissionPovertyNavNode;"Poverty"
;;FLMissionSocialInclusionNavNode;"Social inclusion"
;;FLMissionSupportingOtherSocialEnterprisesNavNode;"Supporting other social enterprises/organisations"
;;FLMissionYouthDevAndOppNavNode;"Youth development and opportunities"

;;AccessoriesNavNode;""
;;AccessoryBrandLinksNavNode;""
;;AccountAddressBookNavNode;"Address Book"
;;AccountLeftNavNode;"My Account"
;;AccountOrderHistoryNavNode;"Order History"
;;AccountPaymentDetailsNavNode;"Payment Details"
;;AccountProfileNavNode;"Profile"
;;BrandLinksNavNode;""
;;BrandsNavNode;"Trading Activity"
;;CameraAccessoriesNavNode;"Camera Accessories & Supplies"
;;CamerasNavNode;"Cameras"
;;DigitalCamerasNavNode;""
;;ElectronicsNavNode;"Electronics Site"
;;FilmCamerasNavNode;"Film Cameras"
;;FlashMemoryNavNode;"Flash Memory"
;;FollowUsNavNode;"Follow Us"
;;HandheldCamcordersNavNode;"Hand Held Camcorders"
;;PowerSuppliesNavNode;"Power Supplies"
;;SAPCommerceNavNode;"SAP Commerce Cloud"
;;SAPCustomerExperienceNavNode;"SAP Customer Experience"
;;SiteRootNode;"SiteRoot"
;;SuppliesNavNode;""
;;WebcamsNavNode;"Webcams"

# FAQ Page

# CMS Paragraph Component (Contact information)
UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en];


# Terms and Conditions Page

# CMS Paragraph Component (Contact information)
UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en];


# Homepage

# Media Content
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];&imageRef;folder(qualifier)[default='images'];altText


# CMS Banner Components
UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];altText;&imageRef

UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]

UPDATE BannerComponent;$contentCV[unique=true];uid[unique=true];headline[lang=$lang];$picture[lang=$lang];content[lang=$lang]

UPDATE ProductCarouselComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang]


# Camera Accessories and Supplies Category Landing Page

UPDATE CategoryPage;$contentCV[unique=true];uid[unique=true];title[lang=$lang]

INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];folder(qualifier)[default='images'];altText

UPDATE CategoryFeatureComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang];$picture[lang=$lang];description[lang=$lang]

UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]

# Search Results Page

# Media Content
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];folder(qualifier)[default='images'];altText

UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]

UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang]

# Cart Page

# Media Content
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];folder(qualifier)[default='images'];altText

UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]

UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang]


