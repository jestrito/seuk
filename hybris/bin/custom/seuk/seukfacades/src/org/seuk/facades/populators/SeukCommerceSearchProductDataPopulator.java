/**
 *
 */
package org.seuk.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;

import org.springframework.util.Assert;


/**
 * @author dsolon
 *
 */
public class SeukCommerceSearchProductDataPopulator implements Populator<SearchResultValueData, ProductData>
{

	@Override
	public void populate(final SearchResultValueData source, final ProductData target) throws ConversionException
	{
		// XXX Auto-generated method stub
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		// Pull the values directly from the SearchResult object
		target.setMission(this.<String> getValue(source, "mission"));
		target.setStreet(this.<String> getValue(source, "street"));
		target.setLine1(this.<String> getValue(source, "line1"));
		target.setLine2(this.<String> getValue(source, "line2"));
		target.setLine3(this.<String> getValue(source, "line3"));
		target.setCity(this.<String> getValue(source, "city"));
		target.setCounty(this.<String> getValue(source, "county"));
		target.setPostCode(this.<String> getValue(source, "postCode"));
		target.setCountry(this.<String> getValue(source, "country"));
		target.setRegion(this.<String> getValue(source, "region"));
		target.setMarket(this.<String> getValue(source, "market"));
		target.setWebsite(this.<String> getValue(source, "website"));
		target.setHeadOffice(this.<String> getValue(source, "city") + ", " + this.<String> getValue(source, "county") + ", "
				+ this.<String> getValue(source, "region"));
		final ArrayList<String> categories = this.<ArrayList<String>> getValue(source, "categoryName");
		//final ArrayList<String> categories = (ArrayList<String>) Arrays.asList(Cats.split(","));
		target.setTradingActivity(categories.iterator().next());

	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}

}
