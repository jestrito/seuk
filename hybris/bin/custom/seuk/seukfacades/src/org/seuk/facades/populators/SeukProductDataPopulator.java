/**
 *
 */
package org.seuk.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * @author dsolon
 *
 */
public class SeukProductDataPopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		// XXX Auto-generated method stub
		target.setMission(source.getMission());
		target.setStreet(source.getStreet());
		target.setLine1(source.getLine1());
		target.setLine2(source.getLine2());
		target.setLine3(source.getLine3());
		target.setCity(source.getCity());
		target.setCounty(source.getCounty());
		target.setPostCode(source.getPostCode());
		target.setCountry(source.getCountry());
		target.setRegion(source.getRegion());
		target.setMarket(source.getMarket());
		target.setWebsite(source.getWebsite());
		target.setHeadOffice(source.getCity() + ", " + source.getCounty() + ", " + source.getRegion());
		target.setTradingActivity(source.getSupercategories().iterator().next().getName());
		target.setTelephone(source.getTelephone());
		target.setEmail(source.getEmail());
		target.setContact(source.getBillingSurname() + ", " + source.getBillingFirstname());
		target.setFacebook(source.getFacebook());
		target.setTwitter(source.getTwitter());
	}

}
